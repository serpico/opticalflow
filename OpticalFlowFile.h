#ifndef _OpticalFlowFile_h
#define _OpticalFlowFile_h

#include <iostream>
#include <fstream>
#include "OpticalFlow.h"

using namespace cimg_library;
using namespace std;

class OpticalFlowFile : public OpticalFlow
{
public:
	OpticalFlowFile();
	OpticalFlowFile(const char filename[]);
	~OpticalFlowFile();

	void compute_OF();
protected:
};



#endif

FROM ubuntu:20.04

WORKDIR /app

COPY . /app

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install valgrind && \
    apt-get -y install cmake && \
    apt-get -y install g++ && \
    apt-get -y install git && \
    apt-get -y install libtiff-dev && \
    apt-get -y install libpng-dev && \
    git clone https://gitlab.inria.fr/serpico/motion2d.git && \
    cd motion2d && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install && \
    cd ../../ && \
    pwd && \
    ls && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_BUILD_TYPE=RELEASE && \
    make

ENV PATH="/app/build/:$PATH"    

CMD ["bash"]

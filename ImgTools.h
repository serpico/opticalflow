#ifndef _ImgTools_h
#define _ImgTools_h

#include "CImg.h"
#include <iostream>
#include <vector>

using namespace cimg_library;
using namespace std;

class ImgTools
{
public:
	ImgTools(void);
	~ImgTools(void);

	static CImg<double> gaussian_rescaling(CImg<double> &I, double scale_factor);

	static void rgb2gray(CImg<double> &I);
	static CImg<double> get_rgb2gray(const CImg<double> &I);

	static void get_derivative(CImg<double> &I, CImg<double> &Ix, CImg<double> &Iy, int filter=1);
	static void get_temporal_derivative(CImg<double> &I1, CImg<double> &I2, CImg<double> &Ix, CImg<double> &Iy, double blend=0.5);
	static void get_pix_derivative(CImg<double> &I, double dx, double dy);
	static CImg<double> grad_magnitude(CImg<double> &I);

	static double get_variance(CImg<double> &I);
	static void compute_eigenValues(CImg<double> &tensor, double &lambda1, double &lambda2, double &lambda3);
	static double scalar_prod(CImg<double> &v1, CImg<double> &v2);
	static string imgName(const char* filename);
	static void rescale(CImg<double> &I, double min=0, double max=255);
	static void rescale_2frames(CImg<double> &I1, CImg<double> &I2, double min, double max);
	static void rescale_tif(CImg<double> &I, double min=0, double max=255);
	static CImg<double> get_rescale(const CImg<double> &I, double min=0, double max=255);
	static void structure_texture_rof(CImg<double> &structure, CImg<double> &texture, CImg<double> &I, double theta=0.125, int nb_iters=100, double alp=0.95);
	static double distance(CImg<double> &I1, CImg<double> &I2);
	static double distance_imgs(CImg<double> &I1, CImg<double> &I2, int type);
	static double distance_patchs(CImg<double> &I1, CImg<double> &I2, int x, int y, int dx, int dy, int size, int type, double spatial_var, double color_var);
	static double median(vector<double> &vect);
	static CImg<double> min_var(vector < CImg<double> > & vect_imgs, vector<int> &vect_inds_x, vector<int> &vect_inds_y, int &i, int &j);
	static double vector_mult(CImg<double> &vect1, CImg<double> &vect2);
	static void cimg_mult(CImg<double> &I1, CImg<double> &I2, CImg<double>&I_out);
	static void cimg_mult(CImg<double> &I1, CImg<double> &I2, CImg<double> &I3, CImg<double>&I_out);
	static void occlusions_zero(CImg<double> &u);
	static void gaussian_weights(CImg<double> &I);
	static void inverse_gaussian_weights(CImg<double> &I);
	static void extend(CImg<double> &I, CImg<double> &I_extend, int w=1);
	static bool include(int width, int height, int x, int y);
	static void gray2rgb(CImg<double> &I_gray, CImg<double> &I_rgb);

	// Image equalization
	static void histogram_cumul(CImg<double> &I, CImg<double> &histo, int nb_bins=256, double val_min=0, double val_max=256, bool normalized=1);
	static void histogram_equal(CImg<double> &I);
	static void histogram_specif(CImg<double> &Iref, CImg<double> &I);
	static void midway_equalization(CImg<double> &I1, CImg<double> &I2);
	static void get_midway_equalization(CImg<double> &I1, CImg<double> &I2, CImg<double> &I1_eq, CImg<double> &I2_eq);
	static void get_midway_equalization(CImgList<double> &I, CImgList<double> &I_eq, int N=5);
	static void get_midway_equalization_tiff(CImg<double> &I, CImg<double> &I_eq, int N=5);

	static void threshold_min(CImg<double> &I, double threshold);
	static void compute_bilateral_weights(CImg<double> &I, double alpha, double beta, CImg<double> &w);
	static void compute_bilateral_weights(CImg<double> &u, CImg<double> &v, double alpha, double beta, CImg<double> &w);
	static void set_gaussian_weights(CImg<double> &I, double std);

	// Warping
	static void warp(CImg<double>& src, CImg<double>& u, CImg<double>& v, string interp_method = "bicubic");
	static CImg<double> get_warp(CImg<double>& src, CImg<double>& u, CImg<double>& v, string interp_method = "bicubic");

	static CImg<double> get_warp_ipol(CImg<double> &input_img,	CImg<double> &u_img, CImg<double> &v_img, CImg<double> &mask_img, const char *type);
	static void bilinear_interpolation(CImg<double> &input_img, CImg<double> &u_img, CImg<double> &v_img, CImg<double> &mask, CImg<double> &output_img);
	static void bicubic_interpolation(CImg<double> &input_img, CImg<double> &u_img, CImg<double> &v_img, CImg<double> &mask, CImg<double> &output_img);
	static int neumann_bc_ipol(int x, int nx, double *mask);
	static int periodic_bc_ipol(int x, int nx, double *mask);
	static int symmetric_bc_ipol(int x, int nx, double *mask);
	static double bicubic_interpolation(const double *input, const double uu, const double vv, int nx, int ny, double &mask);
	static double bicubic_interpolation(double p[4][4], double x, double y);
	static double cubic_interpolation(double v[4], double x);
};

#endif



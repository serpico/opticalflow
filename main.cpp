#include "OpticalFlowBrox.h"
#include "OpticalFlowFile.h"
#include "OpticalFlowMotion2D.h"
#include "ImgTools.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "DisplayOF.h"
#include "CImg.h"

using namespace cimg_library;
//using namespace std;

int main(int argc,char** argv)
{

	cimg_usage("optical_flow usage:");


	/////////////////////////     Parameters    /////////////////////////////////////
	//image sequence
	const char *I1_filename = cimg_option("-f1",(char *)0,"Input frame1 file");
	const char *I2_filename = cimg_option("-f2",(char *)0,"Input frame2 file");

	std::cout << "read I1 en I2" << std::endl;

	// Parameters of the Brox method
	const float coeff_reg = cimg_option("-coeff_reg", 15.F, "Brox parameter : Weight of the global regularization term");
	const float coeff_grad = cimg_option("-coeff_grad", float(3.0), "Brox parameter : Weight of the gradient conservation term");
	const int pyramid_levels = cimg_option("-pyramid_levels", int(40), "Brox parameter : Number of pyramid levels (effective only if min_size_pyramid=0)");
	const float pyramid_ratio = cimg_option("-pyramid_ratio", float(0.95), "Brox parameter : Size scale between two pyramid levels");
	const float eps = cimg_option("-eps", float(0.001), "Brox parameter : Epsilon of the robust L1 norm");
	const int res_iter = cimg_option("-res_iter", int(50), "Brox parameter : Number of iteration for the resolution of the linear system");
	const int outer_iter = cimg_option("-outer_iter", int(3), "Brox parameter : Number of outer iterations");
	const int median_size = cimg_option("-med", int(0), "Brox parameter : Size of the median filtering of the flow (if 0, no median filtering)");
	const float min_size_pyramid = cimg_option("-min_size_pyramid", float(10.0), "Brox parameter : Size of the top level of the pyramid for the adaptation of the number of levels (if 0 : no adaptation)");
	const bool texture = cimg_option("-texture", false, "Brox parameter : Use of structure/texture decomposition");
	const float sigma = cimg_option("-sigma", float(0.9), "Brox parameter : Standard deviation of the gaussian kernel used for pre-smoothing of the images");
	const bool norm = cimg_option("-norm", true, "Brox parameter : ");
	const int midway = cimg_option("-midway", int(0), "Brox parameter : ");

	// Load existing flow in .flo format
	//const char* floFilename = cimg_option("-readFlo", "0", "Filename to load a flo file");

	// Error estimation
	const int er = cimg_option("-er", 0, "Error : global error estimation");
	const char* error_filename = cimg_option("-error_filename", "error.txt", "Error : Error files directory");
	const char* refFlowFile = cimg_option("-refFlow", "ground_truth/grove3.flo", "Error : Reference optical flow for error computation");

	// Display
	const bool disp_color = cimg_option("-disp_color", 0, "If on, display colored optical flow");
	const bool disp_arrow = cimg_option("-disp_arrow", 0, "If on, display arrows of optical flow");
	const bool disp_arrow_img = cimg_option("-disp_arrow_img", 0, "If on, display the arrows of optical flow on the first image");
	const int arrow_sampling = cimg_option("-arrow_sampling", 20, "");
	const int arrow_factor= cimg_option("-arrow_factor", 13, "");

	// Save
	const char* save_color_filename = cimg_option("-save_color","0","Filename for the image result");
	const char* save_arrow_filename = cimg_option("-save_arrow","0","Filename for the image result");
	const char* save_arrowimg_filename = cimg_option("-save_arrowimg","0","Filename for the image result");
	const char* saveFloFilename = cimg_option("-saveFlo", "0", "Save filename for the flo file of the result");

	const bool compensate = cimg_option("-compensate", 0, "");

	if ( !I1_filename && !I2_filename){
		return 0;
	}

///////////////////////////////    Optical flow estimation    ///////////////////////////////////
	OpticalFlow *flow;
	flow = new OpticalFlowBrox(coeff_reg, coeff_grad, pyramid_levels, pyramid_ratio, res_iter, outer_iter, eps, median_size, min_size_pyramid, norm, texture, sigma, midway);

	DisplayOF display;
	CImg<double> I1,I2;
	I1.assign(I1_filename);
	I2.assign(I2_filename);

	ImgTools::rgb2gray(I1);
	ImgTools::rgb2gray(I2);
	I1.blur(sigma);
	I2.blur(sigma);

	if(midway == 1)
		ImgTools::midway_equalization(I1,I2);

	if(compensate)
	{
		OpticalFlowMotion2D flow_motion2d(I1,I2);
		flow_motion2d.set_parameters("AC", 1, 0, -1, -1, 4, 0);
		flow_motion2d.compute_OF();
		CImg<double> u,v;
		flow_motion2d.get_flow(u,v);
		ImgTools::warp(I2,u,v);
	}

	flow->set_imgs(I1,I2);
	flow->compute_OF();

	// Error computation
	if(er == 1)	{
		double AAE;
		OpticalFlowFile refFlow(refFlowFile);
		flow->set_refFlow(&refFlow);
		string I1_str = I1_filename;
		string I2_str = I2_filename;
		string error_filename_str = error_filename;
		flow->write_error_header(error_filename_str, I1_str, I2_str);
		flow->error_flow(error_filename_str,1,AAE);
	}

	///////////////////////////////    Save and Display    ///////////////////////////////////

	/////////////////
	//// Display ////
	/////////////////

	// Display Color image
	if(disp_color == true) {
		CImg<float> colorflow;
		colorflow = display.get_colorImg(*flow);
		CImgDisplay disp(colorflow);
		while (!disp.is_key())
			disp.wait();
	}

	// Display Arrow image
	if(disp_arrow == true)
	{
		CImg<float> arrowflow;
		arrowflow = display.get_arrowImg(*flow, 0,0,0,1,arrow_sampling,arrow_factor);
		CImgDisplay disp(arrowflow);
		while (!disp.is_key())
			disp.wait();
	}

	// Display Arrows on the first image
	if(disp_arrow_img == true)
	{
		CImg<float> arrowimgflow;
		arrowimgflow = display.get_arrowImg(*flow, I1, 255,255,0,1,arrow_sampling,arrow_factor);
		CImgDisplay disp(arrowimgflow);
		disp.resize(I1.width(), I1.height());
		while (!disp.is_key())
			disp.wait();
	}
	

	//////////////
	//// Save ////
	//////////////

	// Save color image
	string str_save_color_filename(save_color_filename);
	if(str_save_color_filename.compare("0") != 0)
	{
		// compute color image
		CImg<float> colorflow;
		colorflow = display.get_colorImg(*flow);
		colorflow(0,0,0,0) = 255;
		colorflow(0,0,0,1) = 255;
		colorflow(0,0,0,2) = 255;

		// convert to 8 bits image
		CImg<unsigned char> colorflowChar;
		colorflowChar = colorflow;

		// save
		colorflowChar.save(save_color_filename);
		cout << str_save_color_filename << endl;
	}

	// Save arrow image
	string str_save_arrow_filename(save_arrow_filename);
	if(str_save_arrow_filename.compare("0") != 0)
	{
		// compute arrowflow
		CImg<float> arrowflow;
		arrowflow = display.get_arrowImg(*flow, 0,0,0,1,arrow_sampling,arrow_factor);

		// convert to 8 bits image
		CImg<unsigned char> arrowflowChar;
		arrowflowChar = arrowflow;

		// save
		arrowflowChar.save(save_arrow_filename);
		cout << str_save_arrow_filename << endl;
	}

	// Save arrows superposed to the image
	string str_save_arrowimg_filename(save_arrowimg_filename);
	if(str_save_arrowimg_filename.compare("0") != 0)
	{
		// compute superposed image
		CImg<float> arrowimgflow;
		arrowimgflow = display.get_arrowImg(*flow, I1, 255,255,0,1,arrow_sampling,arrow_factor);

		// convert 8 bits image
		CImg<unsigned char> arrowimgflowChar;
		arrowimgflowChar = arrowimgflow;

		// save
		arrowimgflowChar.save(save_arrowimg_filename);
		cout << str_save_arrowimg_filename << endl;
	}

	// Save OF in the .flo format
	string str_saveFloFilename(saveFloFilename);
	if(str_saveFloFilename.compare("0") != 0)
	{
		flow->writeFlo(saveFloFilename);
	}

	delete flow;

	return 0;
}

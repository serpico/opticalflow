#ifndef _OpticalFlowBrox_h
#define _OpticalFlowBrox_h

#include "CImg.h"
#include "OpticalFlow.h"

class OpticalFlowPatchs;

using namespace cimg_library;

class OpticalFlowBrox : public OpticalFlow
{
public:
	OpticalFlowBrox(const OpticalFlowBrox &brox_OF);
	OpticalFlowBrox	(double coeff_reg=80, double coeff_grad=16, int pyramid_levels=40, double pyramid_ratio=0.95,
					int res_iter=50, int outer_iter=3, double eps=0.001, int median_size=3, double min_size_pyramid=10, bool norm=1, bool texture=1,
					double sigma=0.9, int midway=0);
	OpticalFlowBrox	(CImg<double> &I1, CImg<double> &I2, double coeff_reg=80, double coeff_grad=16, int pyramid_levels=40, double pyramid_ratio=0.95,
			int res_iter=50, int outer_iter=3, double eps=0.001, int median_size=3, double min_size_pyramid=10, bool norm=1, bool texture=1,
			double sigma=0.9, int midway=0);
	~OpticalFlowBrox();

	void set_parameters	(double coeff_reg=80, double coeff_grad=16, int pyramid_levels=40, double pyramid_ratio=0.95,
						int res_iter=50, int outer_iter=3, double eps=0.001, int median_size=3, double min_size_pyramid=10, bool norm=1, bool texture=1,
						double sigma=0.9, int midway=0);	// Without adaptive regularity coeff
	void set_min_size_pyramid(double min_size_pyramid);
	void set_pyramid_levels(double pyramid_levels);
	void set_coeff_reg(double coeff_reg);
	double get_coeff_reg() const;
	double get_coeff_grad() const;
	int get_pyramid_levels() const;
	double get_pyramid_ratio() const;
	double get_eps() const;
	int get_sigma() const;
	int get_outer_iter() const;
	int get_res_iter() const;
	bool get_norm() const;
	int get_median_size() const;
	double get_min_size_pyramid() const;
	bool get_texture() const;
	bool get_midway() const;

	void compute_OF();

	void write_error_header(string &error_filename, string &I1_filename, string &I2_filename);

protected:
	void resolutionProcess_brox(CImg<double> &du, CImg<double> &dv, CImg<double> &I1, CImg<double> &I2, CImg<double> &u, CImg<double> &v, CImg<double> &coeff_reg_img, CImg<double> &mask);
	inline double psi_value(double x, double eps = 0.001);
	void psi_matrix(CImg<double> &output, CImg<double> &I, double eps = 0.001);
	inline double psiDerivative_value(double x, double eps = 0.001);
	inline double psiDerivative2_value(double x, double eps = 0.001);
	void compute_psidash_D1(CImg<double> &psidash_D1, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &u, CImg<double> &v, CImg<double> &normI, bool normalization=0);
	void compute_psidash_D2(CImg<double> &psidash_D2, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyy,  CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &normIx, CImg<double> &normIy, bool normalization=0);
	void compute_psidash2_D1(CImg<double> &psidash_D1, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &u, CImg<double> &v, CImg<double> &normI, bool normalization=0);
	void compute_psidash2_D2(CImg<double> &psidash_D2, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyy,  CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &normIx, CImg<double> &normIy, bool normalization=0);
	void compute_psidash_S(CImg<double> &psidash_S, CImg<double> &u, CImg<double> &v);
	void compute_psidash(CImg<double> &psidash_D1, CImg<double> &psidash_D2, CImg<double> &psidash_S, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyy,  CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &du, CImg<double> &dv, CImg<double> &normI, CImg<double> &normIx, CImg<double> &normIy, bool normalization=0);
	void constructMatrix(CImg<double> &row_out, CImg<double> &col_out, CImg<double> &val_out, CImg<double> &b, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyx, CImg<double> &Iyy, CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &du, CImg<double> &dv, CImg<double> &mask);
	void sor(CImg<double> &x, double error, int iter, bool flag, CImg<double> &rs, CImg<double> &cs, CImg<double> &ss, CImg<double> &b, double w, int max_it, double tol);

	// Parameters
	double coeff_reg; 		// Weight of the regularization term
	double coeff_grad; 		// Weight of the gradient conservation term
	int pyramid_levels;		// Number of pyramid levels
	double pyramid_ratio; 	// Size scale between two pyramid levels
	double eps;				// Epsilon of the robust L1 norm
	int res_iter;			// Number of iteration for the resolution of the linear system
	int outer_iter;			// Number of outer iterations
	int median_size;		// Size of the median filter (if 0, no median filtering)
	double min_size_pyramid;	// Size of the top level of the pyramid for the adaptation of the number of levels (if 0 : no adaptation)
	bool texture;			// Use of structure texture/texture decomposition. If On: coeff_grad=0, coeff_reg=5~10
	double sigma;			// Standard deviation of the gaussian kernel used for pre-smoothing of the images
	bool norm;
	int midway;
};

#endif

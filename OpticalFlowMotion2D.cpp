//#include "OpticalFlowMotion2D.h"
//
//#include <stdio.h>
//#include <stdlib.h>
//#ifdef __SunOS_
//# include <iostream.h>
//#else
//# include <iostream>
//#endif
//#include <ctype.h>
//#include <string>
//
//// Include for Motion2D
//#include <CMotion2DImage.h>
//#include <CMotion2DEstimator.h>
//#include <CMotion2DPyramid.h>
//#include <CMotion2DModel.h>
//#include <CMotion2DWarping.h>
//#include <CReader.h>
//#include <CImageReader.h>
//#include <CMpeg2Reader.h>
//#include <CImageWriter.h>
//#include <FieldVector.h>
//
//using namespace std;
//using namespace cimg_library;
//
//#define GETOPTARGS	"a:b:c:e:f:ghi:j:k:l:m:n:p:q:r:s:t:u:vw:x:y:z?C:F:IR:"
//#define max(a,b) (a>b?a:b)
//
//bool findChar(string ch, char *f)
//{
//  int n = ch.find(f);
//  int size = ch.size();
//  return (n>0 && n<size);
//};
//
//OpticalFlowMotion2D::OpticalFlowMotion2D(CImg<double> &I1, CImg<double> &I2) : OpticalFlow(I1,I2)
//{
//	set_parameters();
//}
//
//OpticalFlowMotion2D::~OpticalFlowMotion2D()
//{
//
//}
//
//void OpticalFlowMotion2D::set_parameters(string model_id, bool var_light, bool model_orig_fixed, double model_row_orig, double model_col_orig, unsigned pyr_nlevels, bool compute_covariance)
//{
//	this->model_id = model_id;
//	this->model_col_orig = model_col_orig;
//	this->model_row_orig = model_row_orig;
//	this->model_orig_fixed = model_orig_fixed;
//	this->var_light = var_light;
//	this->compute_covariance= compute_covariance;
//	this->pyr_nlevels = pyr_nlevels;
//}
//
//double* OpticalFlowMotion2D::get_coeffs()
//{
//	return coeffs;
//}
//
//void OpticalFlowMotion2D::compute_OF()
//{
//	int width = I1.width(), height = I1.height();
//
//	// Convert CImg into CMotion2DImage
//	CMotion2DImage<short> I1_M2D(height, width), I2_M2D(height, width);
//	cimg_forXY(I1,x,y)
//	{
//		I1_M2D[y][x] = I1(x,y);
//		I2_M2D[y][x] = I2(x,y);
//	}
//	cout << "width = "<< width << "; height = " << height << endl;
//
//	// Initialisation of the 2D parametric motion model to estimate
//	CMotion2DModel	model;		// Parametric motion model
//	model.reset();	// Set all the parameters to zero
//	model.setIdModel(model_id); // Set the model id
//	model.setVarLight(var_light); // Set the illumination variation estimation
//
//	// Initialize the model origin
//	if ( ! model_orig_fixed)
//	{
//		// Origin fixed at the middle of the image
//		model_row_orig = height / 2.0;
//		model_col_orig = width / 2.0;
//	}
//	model.setOrigin(model_row_orig, model_col_orig); // Set the origin
//
//	// Pyramids allocation for two successive images
//	CMotion2DPyramid pyramid1;	// Pyramid on image 1
//	CMotion2DPyramid pyramid2;	// Pyramid on image 2
//	pyramid1.allocate(height, width, pyr_nlevels);
//	pyramid2.allocate(height, width, pyr_nlevels);
//
//	// Fill the pyramids 1 and 2
//	pyramid1.build(I1_M2D.bitmap);
//	pyramid2.build(I2_M2D.bitmap);
//
//	// Initialize the motion estimator
//	// Set the pyramid start estimation level
//	CMotion2DEstimator	estimator;	// Motion estimator
//	bool ierr = true;		// Image IO return value
//	ierr = estimator.setFirstEstimationLevel(pyr_nlevels - 1);
//	if (ierr == false)
//		exit(-1);
//	// Set the pyramid stop estimation level
//	ierr = estimator.setLastEstimationLevel(0);
//	if (ierr == false)
//		exit(-1);
//	estimator.setRobustEstimator(true);
//
//	if (compute_covariance)
//		estimator.computeCovarianceMatrix(true);
//
//	// Initialize the support
//	CMotion2DImage<unsigned char> S;	// Motion estimator support
//	unsigned char label = 234;	// Value of the motion estimator support
//	S.Init(height, width, label); // Set the estimator support to the whole image
//
//	// 2D Parametric motion estimation
//	bool state = estimator.estimate(pyramid1, pyramid2, S.bitmap,
//			 label, model, 0);
//
//	if (state == false)
//	{
//		cout << "\nError during motion estimation. " << endl
//		 << "Possible reason: not enought gradient." << endl
//		 << "The model is set to zero..." << endl << endl;
//	}
//
//	double coeff[16];
////	model.getParameters(coeff);
////	cout << "Coeffs : " << endl;
////	cout << "1  : " << coeff[0] << endl;
////	cout << "2  : " << coeff[1] << endl;
////	cout << "3  : " << coeff[2] << endl;
////	cout << "4  : " << coeff[3] << endl;
////	cout << "5  : " << coeff[4] << endl;
////	cout << "6  : " << coeff[5] << endl;
////	cout << "7  : " << coeff[6] << endl;
////	cout << "8  : " << coeff[7] << endl;
////	cout << "9  : " << coeff[8] << endl;
////	cout << "10 : " << coeff[9] << endl;
////	cout << "11 : " << coeff[10] << endl;
////	cout << "12 : " << coeff[11] << endl;
////	cout << "13 : " << coeff[12] << endl;
////	cout << "14 : " << coeff[13] << endl;
////	cout << "15 : " << coeff[14] << endl;
////	cout << "16 : " << coeff[15] << endl;
//
//	cimg_forXY(u,x,y)
//		model.getDisplacement(y,x,v(x,y), u(x,y));
//}

#include "OpticalFlowMotion2D.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif
#include <ctype.h>
#include <string>

// Include for Motion2D
#include <CMotion2DImage.h>
#include <CMotion2DEstimator.h>
#include <CMotion2DPyramid.h>
#include <CMotion2DModel.h>
#include <CMotion2DWarping.h>
#include <CReader.h>
#include <CImageReader.h>
#include <CMpeg2Reader.h>
#include <CImageWriter.h>
#include <FieldVector.h>

using namespace std;
using namespace cimg_library;

#define GETOPTARGS	"a:b:c:e:f:ghi:j:k:l:m:n:p:q:r:s:t:u:vw:x:y:z?C:F:IR:"
#define max(a,b) (a>b?a:b)

bool findChar(string ch, const char *f)
{
  int n = ch.find(f);
  int size = ch.size();
  return (n>0 && n<size);
}

OpticalFlowMotion2D::OpticalFlowMotion2D(CImg<double> &I1, CImg<double> &I2) : OpticalFlow(I1,I2)
{
	set_parameters();
}

//OpticalFlowMotion2D::OpticalFlowMotion2D(const OpticalFlowParam &params)
//{
//	set_parameters(params);
//	this->type_OF = "0";
//
//}

OpticalFlowMotion2D::~OpticalFlowMotion2D()
{

}

void OpticalFlowMotion2D::set_parameters(string model_id, bool var_light, bool model_orig_fixed, double model_row_orig, double model_col_orig, unsigned pyr_nlevels, bool compute_covariance, const char *robust)
{
	this->model_id = model_id;
	this->model_col_orig = model_col_orig;
	this->model_row_orig = model_row_orig;
	this->model_orig_fixed = model_orig_fixed;
	this->var_light = var_light;
	this->compute_covariance= compute_covariance;
	this->pyr_nlevels = pyr_nlevels;
	this->robust = robust;
}

//void OpticalFlowMotion2D::set_parameters(const OpticalFlowParam &params)
//{
//	set_parameters(params.get_model_id(), params.get_var_light(), params.get_model_orig_fixed(), params.get_model_row_orig(), params.get_model_col_orig(), params.get_pyr_nlevels(), params.get_compute_covariance(), params.get_robust());
//}

double* OpticalFlowMotion2D::get_coeffs()
{
	return coeffs;
}

void OpticalFlowMotion2D::compute_OF()
{
	int width = I1.width(), height = I1.height();

	// Convert CImg into CMotion2DImage
	CMotion2DImage<short> I1_M2D(height, width), I2_M2D(height, width);
	cimg_forXY(I1,x,y)
	{
		I1_M2D[y][x] = I1(x,y);
		I2_M2D[y][x] = I2(x,y);
	}

	// Initialisation of the 2D parametric motion model to estimate
	CMotion2DModel	model;		// Parametric motion model
	model.reset();	// Set all the parameters to zero
	model.setIdModel(model_id); // Set the model id
	model.setConstVarLight(var_light); // Set the illumination variation estimation

	// Initialize the model origin
	if ( ! model_orig_fixed)
	{
		// Origin fixed at the middle of the image
		model_row_orig = height / 2.0;
		model_col_orig = width / 2.0;
	}
	model.setOrigin(model_row_orig, model_col_orig); // Set the origin

	// Pyramids allocation for two successive images
	CMotion2DPyramid pyramid1;	// Pyramid on image 1
	CMotion2DPyramid pyramid2;	// Pyramid on image 2
	pyramid1.allocate(height, width, pyr_nlevels);
	pyramid2.allocate(height, width, pyr_nlevels);

	// Fill the pyramids 1 and 2
	pyramid1.build(I1_M2D.bitmap);
	pyramid2.build(I2_M2D.bitmap);

	// Initialize the motion estimator
	// Set the pyramid start estimation level
	CMotion2DEstimator	estimator;	// Motion estimator
	bool ierr = true;		// Image IO return value
	ierr = estimator.setFirstEstimationLevel(pyr_nlevels - 1);
	if (ierr == false)
		exit(-1);
	// Set the pyramid stop estimation level
	ierr = estimator.setLastEstimationLevel(0);
	if (ierr == false)
		exit(-1);
	if(strcmp(robust, "LS") == 0)
		estimator.setRobustEstimator(false);
	else
	{
		estimator.setRobustEstimator(true);
		if(strcmp(robust,"tukey") ==0)	estimator.setRobustFunction(CMotion2DEstimator::Tukey);
		if(strcmp(robust,"talwar") ==0)	estimator.setRobustFunction(CMotion2DEstimator::Talwar);
		if(strcmp(robust,"cauchy") ==0)	estimator.setRobustFunction(CMotion2DEstimator::Cauchy);
		if(strcmp(robust,"welch") ==0)	estimator.setRobustFunction(CMotion2DEstimator::Welsh);
	}

	if (compute_covariance)
		estimator.computeCovarianceMatrix(true);

	// Initialize the support
	CMotion2DImage<unsigned char> S;	// Motion estimator support
	unsigned char label = 234;	// Value of the motion estimator support
	S.Init(height, width, label); // Set the estimator support to the whole image

	// 2D Parametric motion estimation
	bool state = estimator.estimate(pyramid1, pyramid2, S.bitmap,
			 label, model, 0);

	if (state == false)
	{
		cout << "\nError during motion estimation. " << endl
		 << "Possible reason: not enought gradient." << endl
		 << "The model is set to zero..." << endl << endl;
	}

	double coeff[16];
//	model.getParameters(coeff);
//	cout << "Coeffs : " << endl;
//	cout << "1  : " << coeff[0] << endl;
//	cout << "2  : " << coeff[1] << endl;
//	cout << "3  : " << coeff[2] << endl;
//	cout << "4  : " << coeff[3] << endl;
//	cout << "5  : " << coeff[4] << endl;
//	cout << "6  : " << coeff[5] << endl;
//	cout << "7  : " << coeff[6] << endl;
//	cout << "8  : " << coeff[7] << endl;
//	cout << "9  : " << coeff[8] << endl;
//	cout << "10 : " << coeff[9] << endl;
//	cout << "11 : " << coeff[10] << endl;
//	cout << "12 : " << coeff[11] << endl;
//	cout << "13 : " << coeff[12] << endl;
//	cout << "14 : " << coeff[13] << endl;
//	cout << "15 : " << coeff[14] << endl;
//	cout << "16 : " << coeff[15] << endl;

	cimg_forXY(u,x,y)
		model.getDisplacement(y,x,v(x,y), u(x,y));
}


/*
 * DisplayOF.cpp
 *
 *  Created on: 8 févr. 2011
 *      Author: dfortun
 */

#include "DisplayOF.h"
#include "OpticalFlow.h"
#include "CImg.h"
#include <stdlib.h>
#include <math.h>
#include "ImgTools.h"

using namespace cimg_library;
using namespace std;

typedef unsigned char uchar;

#define OCCLUSION 1666666752

DisplayOF::DisplayOF()
{
	ncols = 0;
	display.assign();
}

DisplayOF::~DisplayOF()
{
}

CImg<double> DisplayOF::get_display()
{
	return display;
}

void DisplayOF::setcols(int r, int g, int b, int k)
{
    colorwheel[k][0] = r;
    colorwheel[k][1] = g;
    colorwheel[k][2] = b;
}

void DisplayOF::makecolorwheel()
{
    // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow
    //  than between yellow and green)
    int RY = 15;
    int YG = 6;
    int GC = 4;
    int CB = 11;
    int BM = 13;
    int MR = 6;
    ncols = RY + YG + GC + CB + BM + MR;
    //printf("ncols = %d\n", ncols);
    if (ncols > MAXCOLS)
	exit(1);
    int i;
    int k = 0;
    for (i = 0; i < RY; i++) setcols(255,	   255*i/RY,	 0,	       k++);
    for (i = 0; i < YG; i++) setcols(255-255*i/YG, 255,		 0,	       k++);
    for (i = 0; i < GC; i++) setcols(0,		   255,		 255*i/GC,     k++);
    for (i = 0; i < CB; i++) setcols(0,		   255-255*i/CB, 255,	       k++);
    for (i = 0; i < BM; i++) setcols(255*i/BM,	   0,		 255,	       k++);
    for (i = 0; i < MR; i++) setcols(255,	   0,		 255-255*i/MR, k++);
}

void DisplayOF::computeColor(float fx, float fy, uchar *pix)
{
	if (ncols == 0)
		makecolorwheel();

    float rad = sqrt(fx * fx + fy * fy);
    float a = atan2(-fy, -fx) / M_PI;
    float fk = (a + 1.0) / 2.0 * (ncols-1);
    int k0 = (int)fk;
    int k1 = (k0 + 1) % ncols;
    float f = fk - k0;
//    f = 0; // uncomment to see original color wheel
    for (int b = 0; b < 3; b++)
    {
		float col0 = colorwheel[k0][b] / 255.0;
		float col1 = colorwheel[k1][b] / 255.0;
		float col = (1 - f) * col0 + f * col1;
		if (rad <= 1)
			col = 1 - rad * (1 - col); // increase saturation with radius
		else
			col *= .75; // out of range
		pix[b] = (int)(255.0 * col);
    }
}

double DisplayOF::findRadmax(CImg<double> &u, CImg<double> &v)
{
	double rad, radmax = 0;
	cimg_forXY(u,x,y)
	{
		if(u(x,y) != OCCLUSION) // si (x,y) n'est pas une occlusion
		{
			rad = sqrt(u(x,y) * u(x,y) + v(x,y) * v(x,y));
			if(rad > radmax)
				radmax = rad;
		}
	}
	return radmax;
}

void DisplayOF::colorImg(CImg<double> &u, CImg<double> &v)
{
	display.assign(u.width(), u.height(), 1, 3, 0);

	double radmax = findRadmax(u,v);
	uchar *color = new uchar[3];

	cimg_forXY(display,x,y)
	{
		if(u(x,y) == OCCLUSION) //cas d'une occlusion
		{
			display(x,y,0,0) = 0;
			display(x,y,0,1) = 0;
			display(x,y,0,2) = 0;
		}
		else
		{
			computeColor(u(x,y)/radmax, v(x,y)/radmax, color);
			display(x,y,0,0) = color[0];
			display(x,y,0,1) = color[1];
			display(x,y,0,2) = color[2];
		}
	}
	delete [] color;
}

CImg<double> & DisplayOF::get_colorImg(OpticalFlow &OF)
{
	CImg<double> u(OF.get_u()),v(OF.get_v());
	colorImg(u,v);
	return display;
}

CImg<double> & DisplayOF::get_colorImg(CImg<double> &u, CImg<double> &v)
{
	colorImg(u,v);
	return display;
}

void DisplayOF::arrowImg(CImg<double> &u, CImg<double> &v, int r, int g, int b, const float opacity, const unsigned int sampling, const float factor)
{
	display.assign(u.width(), u.height(), 1, 3, 255);
	CImg<double> uv(u.width(), u.height(), 1, 2, 0);

	cimg_forXY(uv,x,y)
	{
		if(u(x,y) == OCCLUSION)
		{
			uv(x,y,0) = 0;
			uv(x,y,1) = 0;
		}
		else
		{
			uv(x,y,0) = u(x,y);
			uv(x,y,1) = v(x,y);
		}
	}

	const int color[3] = {r,g,b};
	display.draw_quiver(uv, color, opacity, sampling, factor);
}

CImg<double> & DisplayOF::get_arrowImg(CImg<double> &u, CImg<double> &v, int r, int g, int b, const float opacity, const unsigned int sampling, const float factor)
{
	arrowImg(u,v,r,g,b, opacity, sampling, factor);
	return display;
}

CImg<double> & DisplayOF::get_arrowImg(OpticalFlow &OF, int r, int g, int b, const float opacity, const unsigned int sampling, const float factor)
{
	CImg<double> u(OF.get_u()),v(OF.get_v());
	arrowImg(u,v,r,g,b, opacity, sampling, factor);
	return display;
}

void DisplayOF::arrowImg(CImg<double> &u, CImg<double> &v, CImg<double> &imfond, int r, int g, int b, const float opacity, const unsigned int sampling, const float factor)
{
	CImg<double> imfond_rgb(imfond);
	if(imfond.spectrum() == 1) // Convert to color image
		ImgTools::gray2rgb(imfond, imfond_rgb);

	display.assign(imfond_rgb);
	CImg<double> uv(u.width(), u.height(), 1, 2, 0);

	cimg_forXY(uv,x,y)
	{
		if(u(x,y) == OCCLUSION)
		{
			uv(x,y,0) = 0;
			uv(x,y,1) = 0;
		}
		else
		{
			uv(x,y,0) = u(x,y);
			uv(x,y,1) = v(x,y);
		}
	}

	const int color[3] = {r,g,b};
	display.draw_quiver(uv, color, opacity, sampling, factor);
}

CImg<double> & DisplayOF::get_arrowImg(CImg<double> &u, CImg<double> &v, CImg<double> &imfond, int r, int g, int b, const float opacity, const unsigned int sampling, const float factor)
{
	arrowImg(u,v,imfond,r,g,b, opacity, sampling, factor);
	return display;
}

CImg<double> & DisplayOF::get_arrowImg(OpticalFlow &OF, CImg<double> &imfond, int r, int g, int b, const float opacity, const unsigned int sampling, const float factor)
{
	CImg<double> u(OF.get_u()),v(OF.get_v());
	arrowImg(u,v,imfond,r,g,b, opacity, sampling, factor);
	return display;
}


void DisplayOF::magnitudeImg(CImg<double> &u, CImg<double> &v, int min, int max)
{
	display.assign(u.width(), u.height(), 1, 1, 0);

	cimg_forXY(display,x,y)
		display(x,y,0) = u(x,y)*u(x,y) + v(x,y)*v(x,y);

	if(min > 0 || max < 255)
	{
		cimg_forXY(display,x,y)
		{
			if(display(x,y,0) < min)
				display(x,y,0) = min;

			if(display(x,y,0) > max)
				display(x,y,0) = max;
		}
	}
}

CImg<double> & DisplayOF::get_magnitudeImg(CImg<double> &u, CImg<double> &v, int min, int max)
{
	magnitudeImg(u,v,min,max);
	return display;
}

#include "OpticalFlow.h"
#include "ImgTools.h"
#include <cstdlib>
#include <iostream>
#include <math.h>
#include <sstream>
#include <fstream>

#define TAG_FLOAT 202021.25
#define TAG_STRING "PIEH"
#define OCCLUSION 1666666752


const double pi = 3.141592;

using namespace std;
using namespace cimg_library;

//*********************************    Constructeurs - destructeur    ********************************************//

OpticalFlow::OpticalFlow()
{
	refFlow = NULL;
}

OpticalFlow::OpticalFlow(CImg<double> &I1, CImg<double> &I2)
{
	this->I1 = I1;
	this->I2 = I2;
	u.assign(I1.width(), I1.height(), I1.depth(), 1, 0);
	v.assign(u);
	refFlow = NULL;
	stack_tif = 0;
}

OpticalFlow::OpticalFlow(CImg<double> &I1, CImg<double> &I2, bool stack_tif)
{
	this->I1 = I1;
	this->I2 = I2;
	u.assign(I1.width(), I1.height(),  I1.depth(), 1, 0);
	v.assign(u);
	refFlow = NULL;
	this->stack_tif = stack_tif;

}
OpticalFlow::OpticalFlow(const char filename[])
{
//	if(stack_tif == 0)
		readFlo(filename);
//	if(stack_tif == 1)
//		readFlo_stack(filename);
	refFlow = NULL;
}

OpticalFlow::~OpticalFlow()
{
}

//******************************************    getters/setters    **********************************************//

CImg<double> OpticalFlow::getWarpedImg(CImg<double> &img)
{
	return ImgTools::get_warp(img, u, v);
}


void OpticalFlow::get_flow(CImg<double> &u, CImg<double> &v)
{
	u = this->u;
	v = this->v;
}

CImg<double> OpticalFlow::get_u() const
{
	return u;
}

CImg<double> OpticalFlow::get_v() const
{
	return v;
}

CImg<double> OpticalFlow::get_I1() const
{
	return I1;
}

CImg<double> OpticalFlow::get_I2() const
{
	return I2;
}

char * OpticalFlow::get_method() const
{
	return method;
}

char* OpticalFlow::get_type_OF() const
{
	return type_OF;
}

OpticalFlow * OpticalFlow::get_refFlow() const
{
	return refFlow;
}

bool OpticalFlow::get_stack_tif() const
{
	return stack_tif;
}

void OpticalFlow::set_flow(CImg<double> u, CImg<double> v)
{
	this->u.assign(u);
	this->v.assign(v);
}

void OpticalFlow::set_imgs(CImg<double> I1, CImg<double> I2)
{
	this->I1 = I1;
	this->I2 = I2;
	u.assign(I1.width(), I1.height(), I1.depth(), 1, 0);
	v.assign(u);
}

void OpticalFlow::set_refFlow(OpticalFlow *refFlow)
{
	this->refFlow = refFlow;
}

void OpticalFlow::set_type_OF(char *type_OF)
{
	this->type_OF = type_OF;
}

void OpticalFlow::set_xypatch(int xpatch, int ypatch)
{
	this->xpatch  =xpatch;
	this->ypatch  =ypatch;
}

void OpticalFlow::set_stack_tif(bool stack_tif)
{
	this->stack_tif = stack_tif;
}

/**************************		Confidence measures	     ***********************************/
void OpticalFlow::confidence_img(CImg<double> confidence_img)
{

}

double OpticalFlow::confidence_global()
{

}

/* confidence_type:
 * 1: variance
 */
//double OpticalFlow::confidence_measure(CImg<double> u, CImg<double> v, int confidence_type)
//{
//	double confidence;
//	if(confidence_type == 1)
//	{
//		CImg<double> magnitude_flow;
//		magnitude_flow = u.pow(2) + v.pow(2);
//		confidence = magnitude_flow.variance();
//	}
//	return confidence;
//}

//*********************************************    error estimation    *******************************************//
void OpticalFlow::error_flow(string &error_filename, bool display, double &AAE)
{
	double *taberrorAE = new double[5];
	double *taberrorEP = new double[5];

	errorAE(*refFlow, taberrorAE);
	errorEP(*refFlow, taberrorEP);

	if(error_filename.compare("0") != 0)
		write_error_global(taberrorAE, taberrorEP, error_filename);

	if(display)
	{
		cout << "AAE global = " << taberrorAE[0] << endl;
		cout << "AEP global = " << taberrorEP[0] << endl;
	}

	AAE = taberrorAE[0];

	delete [] taberrorAE;
	delete [] taberrorEP;
}

/////////////////// error images

//Angular error
void OpticalFlow::errorImgAE(CImg<double> &errorImg, OpticalFlow &refOF)
{
	CImg<double> uref,vref, vectEst(3,1,1,1), vectRef(3,1,1,1);
	refOF.get_flow(uref,vref);

	errorImg.assign(uref.width(), uref.height(),1,1);

	cimg_forXY(errorImg,x,y)
	{
		if(uref(x,y) != OCCLUSION && u(x,y) != OCCLUSION) // if the pixel is not an occlusion
		{
			vectEst(0) = u(x,y); vectEst(1) = v(x,y); vectEst(2) = 1; vectEst /= sqrt(u(x,y)*u(x,y) + v(x,y)*v(x,y) + 1);
			vectRef(0) = uref(x,y); vectRef(1) = vref(x,y); vectRef(2) = 1; vectRef /= sqrt(uref(x,y)*uref(x,y) + vref(x,y)*vref(x,y) + 1);
			errorImg(x,y) = acos(ImgTools::scalar_prod(vectEst, vectRef))*180/pi;
		}
		else
			errorImg(x,y) = -1;
	}
}

void OpticalFlow::errorImgEP(CImg<double> &errorImg, OpticalFlow &refOF)
{
	CImg<double> uref,vref;
	refOF.get_flow(uref,vref);

	errorImg.assign(uref.width(), uref.height(),1,1);

	cimg_forXY(errorImg,x,y)
		if(uref(x,y) != OCCLUSION && u(x,y) != OCCLUSION) // if the pixel is not an occlusion
			errorImg(x,y) = sqrt(pow(u(x,y)-uref(x,y),2) + pow(v(x,y)-vref(x,y),2));
		else
			errorImg(x,y) = -1;
}

/////////////////////  error values

//ANGULAR ERROR
double OpticalFlow::errorValAE(OpticalFlow &refOF, int type)
{
	CImg<double> errorImg;
	errorImgAE(errorImg, refOF);

	//error statistics
	switch(type)
	{
	case 1 : //average
		return mean_error(errorImg);

	case 2 : // standard deviation
		return sqrt(errorImg.variance());

	case 3:
		return Rstat(errorImg, 2.5);

	case 4:
		return Rstat(errorImg, 5);

	case 5:
		return Rstat(errorImg, 10);
	}
}

double OpticalFlow::errorValAE(OpticalFlow &refOF, int type, CImg<double> &errorImg)
{
	errorImgAE(errorImg, refOF);

	//error statistics
	switch(type)
	{
	case 1 : //avearge
		return mean_error(errorImg);

	case 2 : // standard deviation
		return sqrt(errorImg.variance());

	case 3:
		return Rstat(errorImg, 2.5);

	case 4:
		return Rstat(errorImg, 5);

	case 5:
		return Rstat(errorImg, 10);

	}
}

//ENDPOINT ERROR
double OpticalFlow::errorValEP(OpticalFlow &refOF, int type)
{
	CImg<double> errorImg;
	errorImgEP(errorImg, refOF);

	//error statistics
	switch(type)
	{
	case 1 : //average
		return mean_error(errorImg);

	case 2 : // standard deviation
		return sqrt(errorImg.variance());

	case 3:
		return Rstat(errorImg, 0.5);

	case 4:
		return Rstat(errorImg, 1);

	case 5:
		return Rstat(errorImg, 2);

	}
}

double OpticalFlow::errorValEP(OpticalFlow &refOF, int type, CImg<double> &errorImg)
{
	errorImgEP(errorImg, refOF);

	//error statistics
	switch(type)
	{
	case 1 : //average
		return mean_error(errorImg);

	case 2 : // standard deviation
		return sqrt(errorImg.variance());

	case 3:
		return Rstat(errorImg, 0.5);

	case 4:
		return Rstat(errorImg, 1);

	case 5:
		return Rstat(errorImg, 2);

	}

}

double OpticalFlow::mean_error(CImg<double> &I_error)
{
	double sum = 0, nb_pixs = 0;
	cimg_forXY(I_error,x,y)
	{
		if(I_error(x,y) != -1)
		{
			sum += I_error(x,y);
			nb_pixs ++;
		}
	}
	return sum/nb_pixs;
}

double OpticalFlow::variance_error(CImg<double> &I_error)
{
//	TODO
}

double OpticalFlow::Rstat(CImg<double> &errorImg, double threshold)
{
	int N = 0;
	cimg_forXY(errorImg,x,y)
		if(errorImg(x,y) > threshold)
			N++;
	return N * 100 / (errorImg.width() * errorImg.height());
}

void OpticalFlow::errorAE(OpticalFlow &refOF, double *errorAE)
{
	// 0:average; 1:standard deviation; 2:R2.5; 3:R5; 4:R10
	errorAE[0] = errorValAE(refOF, 1);
	errorAE[1] = errorValAE(refOF, 2);
	errorAE[2] = errorValAE(refOF, 3);
	errorAE[3] = errorValAE(refOF, 4);
	errorAE[4] = errorValAE(refOF, 5);
}

void OpticalFlow::errorEP(OpticalFlow &refOF, double *errorEP)
{
	// 0:average; 1:standard deviation; 2:R0.5; 3:R1; 4:R2
	errorEP[0] = errorValEP(refOF, 1);
	errorEP[1] = errorValEP(refOF, 2);
	errorEP[2] = errorValEP(refOF, 3);
	errorEP[3] = errorValEP(refOF, 4);
	errorEP[4] = errorValEP(refOF, 5);

}

void OpticalFlow::errorStats(vector< vector<double> > &error, vector< vector<double> > &statError)
{
	int nbPatchs = error.size();
	int nbErrorTypes = error[0].size();
	double valerror;
	statError.resize(2, vector<double>(nbErrorTypes));
	//moyenne : statError[0]
	for(int i=0; i<nbErrorTypes; i++)
	{
		for(int j=0; j<nbPatchs; j++)
		{valerror = error[j][i];
			statError[0][i] += error[j][i];
		}
		statError[0][i] /= nbPatchs;
	}
	//variance : statError[1]
	for(int i=0; i<nbErrorTypes; i++)
	{
		for(int j=0; j<nbPatchs; j++)
			statError[1][i] += pow(error[j][i] - statError[0][i],2);
		statError[1][i] /= (nbPatchs-1);
	}
}

void OpticalFlow::add_patchErrors_in_file(ofstream &errorFile, int centerx, int centery, double *errorAE, double *errorEP)
{
	errorFile << "(" << centerx << "," << centery << ")" << endl;
	errorFile << "# AE" << endl;
	errorFile << "A " << errorAE[0] << " - SD " << errorAE[1] << " - R2.5 " << errorAE[2] << " - R5 " << errorAE[3] << " - R10 " << errorAE[4] << endl;
	if(errorEP != NULL)
	{
		errorFile << "# EP " << endl;
		errorFile << "A " << errorEP[0] << " - SD " << errorEP[1] << " - R0.5 " << errorEP[2] << " - R1 " << errorEP[3] << " - R2 " << errorEP[4] << endl;
	}
	errorFile << endl;
}

void OpticalFlow::add_patchErrorStats_in_file(ofstream &errorFile, vector< vector<double> > &statAE, vector< vector<double> > &statEP)
{
	errorFile << "# error stats :" << endl;

	errorFile << "# AE mean" << endl;
	errorFile << "A " << statAE[0][0] << " - SD " << statAE[0][1] << " - R2.5 " << statAE[0][2] << " - R5 " << statAE[0][3] << " - R10 " << statAE[0][4] << endl;
	errorFile << "# AE variance" << endl;
	errorFile << "A " << statAE[1][0] << " - SD " << statAE[1][1] << " - R2.5 " << statAE[1][2] << " - R5 " << statAE[1][3] << " - R10 " << statAE[1][4] << endl;;

	errorFile << "# EP mean" << endl;
	errorFile << "A " << statEP[0][0] << " - SD " << statEP[0][1] << " - R0.5 " << statEP[0][2] << " - R1 " << statEP[0][3] << " - R2 " << statEP[0][4] << endl;;
	errorFile << "# EP variance" << endl;
	errorFile << "A " << statEP[1][0] << " - SD " << statEP[1][1] << " - R0.5 " << statEP[1][2] << " - R1 " << statEP[1][3] << " - R2 " << statEP[1][4] << endl;;

}

void OpticalFlow::build_error_file(ofstream &errorFile, string &str_erDir, int patchOutSize, int patchInSize, int step, double regGlob, double regLoc, string &str_imgName, int erType, int version_estimParam)
{
	ostringstream ostr_erFilename;
	if(erType == 1)
	{
		//en-tete
		ostr_erFilename << str_erDir << "/errorPatchs_" << str_imgName << "_aGlob" << regGlob << "_aLoc" << regLoc << ".txt";
		cout << "error filename : " <<  ostr_erFilename.str() << endl;

		errorFile.open(ostr_erFilename.str().c_str());

		if(errorFile.is_open())
		{
			errorFile << "# flow errors in patches, before agregation; erType = " << erType << endl;
			errorFile << "# " << str_imgName << "; pacthSize = " << patchOutSize << "; step = " << step << endl;
			errorFile << "# regGlob = " << regGlob << "; regLoc = " << regLoc << "; version_estimParam = " << version_estimParam << endl << endl;
		}
		else
			cout << "unable to open errorFile" << endl;
	}
	if(erType == 2)
	{
		//en-tete
		ostr_erFilename << str_erDir << "/errorPatchs_" << str_imgName << "_aGlob" << regGlob << "_aLoc" << regLoc << ".txt";
		cout << "error filename : " <<  ostr_erFilename.str() << endl;

		errorFile.open(ostr_erFilename.str().c_str());

		if(errorFile.is_open())
		{
			errorFile << "# flow errors in patches, before agregation; erType = " << erType << endl;
			errorFile << "# " << str_imgName << "; pacthOutSize = " << patchOutSize << "; pacthInSize = " << patchInSize << "; step = " << step << endl;
			errorFile << "# regGlob = " << regGlob << "; regLoc = " << regLoc << "; version_estimParam = " << version_estimParam << endl << endl;
		}
		else
			cout << "unable to open errorFile" << endl;
	}

	if(erType == 3)
	{
		//en-tete
		ostr_erFilename << str_erDir << "/errorGlob_" << str_imgName << "_aGlob" << regGlob << "_aLoc" << regLoc << ".txt";
		cout << "error filename : " <<  ostr_erFilename.str() << endl;

		errorFile.open(ostr_erFilename.str().c_str());

		if(errorFile.is_open())
		{
			errorFile << "# global flow error, agregation: mean; erType = " << erType << endl;
			errorFile << "# " << str_imgName << "; pacthOutSize = " << patchOutSize << "; pacthInSize = " << patchInSize << "; step = " << step << endl;
			errorFile << "# regGlob = " << regGlob << "; regLoc = " << regLoc << "; version_estimParam = " << version_estimParam << endl << endl;
		}
		else
			cout << "unable to open errorFile" << endl;

	}

}

void OpticalFlow::write_error_header(string &error_filename, string &I1_filename, string &I2_filename)
{
	ofstream error_file(error_filename.c_str());
	error_file << "# Loaded optical flow" << endl;
}

void OpticalFlow::write_error_global(double *AE_stats, double *EP_stats, string error_filename)
{
	ofstream error_file(error_filename.c_str(), ios::app);

	error_file << "# Global AAE" << endl;
	error_file << AE_stats[0] << endl;
	error_file << endl;

	error_file << "# Global AEP" << endl;
	error_file << EP_stats[0] << endl;
	error_file << endl;

}
//*****************************************    Flo files In/Out    *****************************************//

void OpticalFlow::readFlo(const char filename[])
{
	if (filename == NULL)
		cout << "ReadFlowFile: empty filename" << endl;

	const char *dot = strrchr(filename, '.');
	if (strcmp(dot, ".flo") != 0)
		cout << "ReadFlowFile (%s): extension .flo expected" << endl;

	FILE *stream = fopen(filename, "rb");
	if (stream == 0)
		cout << "ReadFlowFile: could not open %s" << endl;

	int width, height;
	float tag;

	if ((int)fread(&tag, sizeof(float), 1, stream) != 1 || (int)fread(&width, sizeof(int), 1, stream) != 1 || (int)fread(&height, sizeof(int), 1, stream) != 1)
		cout << "ReadFlowFile: problem reading file %s" << endl;

	if (tag != TAG_FLOAT) // simple test for correct endian-ness
		cout << "ReadFlowFile(%s): wrong tag (possibly due to big-endian machine?)" << endl;

	// another sanity check to see that integers were read correctly (99999 should do the trick...)
	if (width < 1 || width > 99999)
		cout << "ReadFlowFile(%s): illegal width %d" << endl;

	if (height < 1 || height > 99999)
		cout << "ReadFlowFile(%s): illegal height %d" << endl;

	int nBands = 2;
	u.assign(width, height, 1, 1); v.assign(u);

	//printf("reading %d x %d x 2 = %d floats\n", width, height, width*height*2);
	int n = nBands * width;
	float *ptr = new float[n];
	for (int y = 0; y < height; y++)
	{
		if ((int)fread(ptr, sizeof(float), n, stream) != n)
			cout << "ReadFlowFile(%s): file is too short" << endl;
		else
			cimg_forX(u,x)
			{
				u(x,y) = ptr[2*x];
				v(x,y) = ptr[2*x+1];
			}
	}
	fclose(stream);
}

void OpticalFlow::readFlo_stack(const char filename[])
{
	if (filename == NULL)
		cout << "ReadFlowFile: empty filename" << endl;

	const char *dot = strrchr(filename, '.');
	if (strcmp(dot, ".flostack") != 0)
		cout << "ReadFlowFile (%s): extension .flostack expected" << endl;

	FILE *stream = fopen(filename, "rb");
	if (stream == 0)
		cout << "ReadFlowFile: could not open %s" << endl;

	int width, height, nb_frames;
	float tag;

	if ((int)fread(&tag, sizeof(float), 1, stream) != 1 || (int)fread(&width, sizeof(int), 1, stream) != 1 || (int)fread(&height, sizeof(int), 1, stream) != 1 || (int)fread(&nb_frames, sizeof(int), 1, stream) != 1)
		cout << "ReadFlowFile: problem reading file %s" << endl;

	if (tag != TAG_FLOAT) // simple test for correct endian-ness
		cout << "ReadFlowFile(%s): wrong tag (possibly due to big-endian machine?)" << endl;

	// another sanity check to see that integers were read correctly (99999 should do the trick...)
	if (width < 1 || width > 99999)
		cout << "ReadFlowFile(%s): illegal width %d" << endl;

	if (height < 1 || height > 99999)
		cout << "ReadFlowFile(%s): illegal height %d" << endl;

	if (nb_frames < 2 || nb_frames > 99999)
		cout << "ReadFlowFile(%s): illegal nb_frames %d" << endl;

	int nBands = 2;
	u.assign(width, height, 1, 1); v.assign(u);

	//printf("reading %d x %d x 2 = %d floats\n", width, height, width*height*2);
	u.assign(width,height,nb_frames,1);v.assign(u);
	int n = nBands * width;
	float *ptr = new float[n];
    for(int z=0; z<nb_frames; z++)
    {
		for (int y = 0; y < height; y++)
		{
			if ((int)fread(ptr, sizeof(float), n, stream) != n)
				cout << "ReadFlowFile(%s): file is too short" << endl;
			else
				cimg_forX(u,x)
				{
					u(x,y,z) = ptr[2*x];
					v(x,y,z) = ptr[2*x+1];
				}
		}
    }
    delete [] ptr;
	fclose(stream);
}

void OpticalFlow::writeFlo_stack(const char *filename)
{ cout << filename << endl;
	if (filename == NULL)
		cout << "WriteFlowFile: empty filename" << endl;

    const char *dot = strrchr(filename, '.');
    if (dot == NULL)
    	cout << "WriteFlowFile: extension required in filename '%s'" << endl;

    if (strcmp(dot, ".flostack") != 0)
    	cout << "WriteFlowFile: filename '%s' should have extension '.flostack'" << endl;

    int width = u.width(), height = u.height(), nb_frames = u.depth(), nBands = 2;

    if (nBands != 2)
    	cout << "WriteFlowFile(%s): image must have 2 bands" << endl;

    FILE *stream = fopen(filename, "wb");
    if (stream == 0)
    	cout << "WriteFlowFile: could not open %s" << endl;

    // write the header
    fprintf(stream, TAG_STRING);
    if ((int)fwrite(&width,  sizeof(int),   1, stream) != 1 ||
	(int)fwrite(&height, sizeof(int),   1, stream) != 1 ||
	(int)fwrite(&nb_frames, sizeof(int),   1, stream) != 1)
    	cout << "WriteFlowFile(%s): problem writing header" << endl;

    // write the rows
    int n = nBands * width;
    float* ptr = new float[n];
    for(int z=0; z<nb_frames; z++)
    {
		for (int y = 0; y < height; y++)
		{
			for(int i=0; i<width; i++)
			{
				ptr[2*i] = u(i,y,z);
				ptr[2*i+1] = v(i,y,z);
			}

			if ((int)fwrite(ptr, sizeof(float), n, stream) != n)
				cout << "WriteFlowFile(%s): problem writing data" << endl;
	   }
    }
    delete [] ptr;
    fclose(stream);
}

void OpticalFlow::writeFlo(const char *filename)
{ cout << filename << endl;
	if (filename == NULL)
		cout << "WriteFlowFile: empty filename" << endl;

    const char *dot = strrchr(filename, '.');
    if (dot == NULL)
    	cout << "WriteFlowFile: extension required in filename '%s'" << endl;

    if (strcmp(dot, ".flo") != 0)
    	cout << "WriteFlowFile: filename '%s' should have extension '.flo'" << endl;

    int width = u.width(), height = u.height(), nBands = 2;

    if (nBands != 2)
    	cout << "WriteFlowFile(%s): image must have 2 bands" << endl;

    FILE *stream = fopen(filename, "wb");
    if (stream == 0)
    	cout << "WriteFlowFile: could not open %s" << endl;

    // write the header
    fprintf(stream, TAG_STRING);
    if ((int)fwrite(&width,  sizeof(int),   1, stream) != 1 ||
	(int)fwrite(&height, sizeof(int),   1, stream) != 1)
    	cout << "WriteFlowFile(%s): problem writing header" << endl;

    // write the rows
    int n = nBands * width;
    float* ptr = new float[n];
    for (int y = 0; y < height; y++)
    {
		for(int i=0; i<width; i++)
		{
			ptr[2*i] = u(i,y);
			ptr[2*i+1] = v(i,y);
		}

		if ((int)fwrite(ptr, sizeof(float), n, stream) != n)
			cout << "WriteFlowFile(%s): problem writing data" << endl;
   }

    fclose(stream);
}

//*********************************************    others    **************************************************//

void OpticalFlow::histogram(CImg<double> &output, CImg<double> &u, CImg<double> &v, int nbLevels)
{
	CImg<double> imVelocity(u);
	cimg_forXY(imVelocity,x,y)
		imVelocity(x,y) = sqrt(u(x,y)*u(x,y) + v(x,y)*v(x,y));

	output = imVelocity.histogram(nbLevels);
}

void OpticalFlow::thresholdVelocity(CImg<double> &u, CImg<double> &v)
{
	float threshold = 2.5;
	int rapport;
	cimg_forXY(u,x,y)
	{
		if(sqrt(u(x,y)*u(x,y) + v(x,y)*v(x,y)) > threshold)
		{
			rapport = (u(x,y)*u(x,y)) / (v(x,y)*v(x,y));
			u(x,y) = u(x,y)/abs(u(x,y)) * sqrt(threshold / (1 + rapport));
			v(x,y) = v(x,y)/abs(v(x,y)) * (threshold - u(x,y));
		}
	}
}

void OpticalFlow::empty()
{
	u.empty();
	v.empty();
}

bool OpticalFlow::is_empty()
{
	return (u.is_empty() && v.is_empty());
}

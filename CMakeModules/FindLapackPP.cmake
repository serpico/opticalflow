# File : FindLapackPP.cmake
# Date : 17/11/2011
# Author : Tristan Lecorgne
# INRIA Serpico team
# 
# Description :
#     Try to find lapackpp library on the system and set the following variables :
#
# LAPACKPP_FOUND
# LAPACKPP_LIBRARIES
# LAPACKPP_INCLUDES
#
# !!!!!! Not working for WINDOWS !!!!!!!
#

if (UNIX OR APPLE)
    find_library (LAPACKPP_LIBRARY
            NAMES lapackpp
            PATHS
            $ENV{LAPACK_HOME}
            $ENV{LAPACK_DIR}
            /usr/lib
            /usr/lib/lapackpp
            /usr/lib/lapackpp/lib
            /usr/lib64
            /usr/lib64/lapackpp
            /usr/local/lib
            /usr/local/lib/lapackpp
            /usr/local/lib64
            /usr/local/lib64/lapackpp
        )
    find_path (LAPACKPP_INCLUDE
                lapackpp.h
                /usr/include
                /usr/include/lapackpp
                /usr/local/include
                /usr/local/include/lapackpp
        )
endif (UNIX OR APPLE)

## --------------------------------
set (LAPACKPP_FOUND FALSE)
if (LAPACKPP_LIBRARY AND LAPACKPP_INCLUDE)
    set (LAPACKPP_LIBRARIES ${LAPACKPP_LIBRARY})
    set (LAPACKPP_INCLUDES ${LAPACKPP_INCLUDE})
    set (LAPACKPP_FOUND TRUE)
endif (LAPACKPP_LIBRARY AND LAPACKPP_INCLUDE)

set (LAPACKPP_LIBRARIES ${LAPACKPP_LIBRARIES} CACHE STRING "lib directories for lapackpp")
set (LAPACKPP_INCLUDES ${LAPACKPP_INCLUDES} CACHE STRING "include directories for lapackpp dependancies")

mark_as_advanced (
  LAPACKPP_LIBRARY
  LAPACKPP_INCLUDE
  LAPACKPP_LIBRARIES
  LAPACKPP_INCLUDES
)

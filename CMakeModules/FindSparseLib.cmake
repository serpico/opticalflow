# File : FindLapackPP.cmake
# Date : 17/11/2011
# Author : Tristan Lecorgne
# INRIA Serpico team
# 
# Description :
#     Try to find sparselib library on the system and set the following variables :
#
# SPARSELIB_FOUND
# SPARSELIB_LIBRARIES
# SPARSELIB_INCLUDES
#
# !!!!!! Not working for WINDOWS !!!!!!!
#

if (UNIX OR APPLE)
# Find libsparse.a
    find_library (SPARSELIB_LIBRARY
            NAMES sparse
            PATHS
            $ENV{SPARSELIB_HOME}
            $ENV{SPARSELIB_DIR}
            /usr/lib
            /usr/lib/sparselib
            /usr/lib/sparselib/lib
            /usr/lib64
            /usr/lib64/sparselib
            /usr/local/lib
            /usr/local/lib/sparselib
            /usr/local/lib64
            /usr/local/lib64/sparselib
        )
  
# Find libmv.a
    find_library (MV_LIBRARY
            NAMES mv
            PATHS
            $ENV{SPARSELIB_HOME}
            $ENV{SPARSELIB_DIR}
            /usr/lib
            /usr/lib/sparselib
            /usr/lib/sparselib/lib
            /usr/lib64
            /usr/lib64/sparselib
            /usr/local/lib
            /usr/local/lib/sparselib
            /usr/local/lib64
            /usr/local/lib64/sparselib
        )
  
# Find libspblas.a
    find_library (SPBLAS_LIBRARY
            NAMES spblas
            PATHS
            $ENV{SPARSELIB_HOME}
            $ENV{SPARSELIB_DIR}
            /usr/lib
            /usr/lib/sparselib
            /usr/lib/sparselib/lib
            /usr/lib64
            /usr/lib64/sparselib
            /usr/local/lib
            /usr/local/lib/sparselib
            /usr/local/lib64
            /usr/local/lib64/sparselib
        )

    find_path (SPARSELIB_INCLUDE
                compcol_double.h
                /usr/include
                /usr/include/sparselib
                /usr/local/include
                /usr/local/include/sparselib
        )
endif (UNIX OR APPLE)

## --------------------------------
set (SPARSELIB_FOUND FALSE)
if (SPARSELIB_LIBRARY AND SPARSELIB_INCLUDE)
    set (SPARSELIB_LIBRARIES ${SPARSELIB_LIBRARY} ${MV_LIBRARY} ${SPBLAS_LIBRARY})
    set (SPARSELIB_INCLUDES ${SPARSELIB_INCLUDE})
    set (SPARSELIB_FOUND TRUE)
endif (SPARSELIB_LIBRARY AND SPARSELIB_INCLUDE)

set (SPARSELIB_LIBRARIES ${SPARSELIB_LIBRARIES} CACHE STRING "lib directories for sparselib")
set (SPARSELIB_INCLUDES ${SPARSELIB_INCLUDES} CACHE STRING "include directories for sparselib dependancies")

mark_as_advanced (
  SPARSELIB_LIBRARY
  MV_LIBRARY
  SPBLAS_LIBRARY
  SPARSELIB_INCLUDE
  SPARSELIB_LIBRARIES
  SPARSELIB_INCLUDES
)

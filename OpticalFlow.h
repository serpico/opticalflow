#ifndef _OpticalFlow_h
#define _OpticalFlow_h

#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>

using namespace cimg_library;
using namespace std;

class OpticalFlow
{
public:
	OpticalFlow();
	OpticalFlow(CImg<double> &I1, CImg<double> &I2);
	OpticalFlow(CImg<double> &I1, CImg<double> &I2, bool stack_tif);
	OpticalFlow(const char filename[]);
	virtual ~OpticalFlow();

	void get_flow(CImg<double> &u, CImg<double> &v);
	CImg<double> get_u() const;
	CImg<double> get_v() const;
	CImg<double> getWarpedImg(CImg<double> &img);
	CImg<double> get_I1() const;
	CImg<double> get_I2() const;
	char * get_method() const;
	char * get_type_OF() const;
	bool get_stack_tif() const;
	OpticalFlow * get_refFlow() const;

	void set_flow(CImg<double> u, CImg<double> v);
	void set_imgs(CImg<double> I1, CImg<double> I2);
	void set_refFlow(OpticalFlow *refFlow);
	void set_type_OF(char *type_OF);
	void set_xypatch(int xpatch, int ypatch);
	void set_stack_tif(bool stack_tif);

	virtual void compute_OF() = 0;
	/**************************		Confidence measures	     ***********************************/
	void confidence_img(CImg<double> confidence_img);
	double confidence_global();

	/****************************    error estimation    ************************************/
	void error_flow(string &error_filename, bool display, double &AAE);

	void errorImgAE(CImg<double> &error, OpticalFlow &refOF); // angular error
	void errorImgEP(CImg<double> &error, OpticalFlow &refOF); // endpoint error

	double errorValAE(OpticalFlow &refOF, int type); // angular error
	double errorValAE(OpticalFlow &refOF, int type, CImg<double> &errorImg);
	double errorValEP(OpticalFlow &refOF, int type); // endpoint error
	double errorValEP(OpticalFlow &refOF, int type, CImg<double> &errorImg);
	double mean_error(CImg<double> &I_error);
	double variance_error(CImg<double> &I_error);
	double Rstat(CImg<double> &errorImg, double threshold);
	void errorAE(OpticalFlow &refOF, double *errorAE);
	void errorEP(OpticalFlow &refOF, double *errorEP);
	void errorStats(vector< vector<double> > &errors, vector< vector<double> > &statError);
	void add_patchErrors_in_file(ofstream &errorFile, int centerx, int centery, double *errorAE, double *errorEP = NULL);
	void add_patchErrorStats_in_file(ofstream &errorFile, vector< vector<double> > &statAE, vector< vector<double> > &statEP);
	void build_error_file(ofstream &errorFile, string &str_erDir, int patchOutSize, int patchInSize, int step, double regGlob, double regLoc, string &str_imgName, int erType, int version_estimParam);
	virtual void write_error_header(string &error_filename, string &I1_filename, string &I2_filename);
	void write_error_global(double *AE_stats, double *EP_stats, string error_filename);

	/********************************    flo files    ******************************************/
	void readFlo(const char filename[]);
	void writeFlo(const char *filename);
	void readFlo_stack(const char *filename);
	void writeFlo_stack(const char *filename);

	/****************************************    others    **************************************/
	void fusion(CImg<double> &u, CImg<double> &v);
	void histogram(CImg<double> &output, CImg<double> &u, CImg<double> &v, int nbLevels);
	void thresholdVelocity(CImg<double> &u, CImg<double> &v);
	void empty();
	bool is_empty();

protected:
	CImg<double> u, v;
	CImg<double> I1, I2;
	OpticalFlow *refFlow;
	char *type_OF;
	char *method;
	bool stack_tif;

	// Block matching parameters
	int xpatch,ypatch;
};



#endif

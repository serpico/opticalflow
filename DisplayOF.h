/*
 * DisplayOF.h
 *
 *  Created on: 8 févr. 2011
 *      Author: dfortun
 */

#ifndef DisplayOF_H_
#define DisplayOF_H_

#include "CImg.h"
class OpticalFlow;

#define MAXCOLS 60
typedef unsigned char uchar;

using namespace cimg_library;

class DisplayOF
{
protected:
	int ncols;
	int colorwheel[MAXCOLS][3];
	CImg<double> display;

public:
	DisplayOF();
	virtual ~DisplayOF();

	CImg<double> get_display();

	//colorwheel
	void setcols(int r, int g, int b, int k);
	void makecolorwheel();
	void computeColor(float fx, float fy, uchar *pix);
	double findRadmax(CImg<double> &u, CImg<double> &v);
	void colorImg(CImg<double> &u, CImg<double> &v);
	CImg<double> & get_colorImg(OpticalFlow &OF);
	CImg<double> & get_colorImg(CImg<double> &u, CImg<double> &v);

	//arrows
	void arrowImg(CImg<double> &u, CImg<double> &v, int r=0, int g=0, int b=0, const float opacity=1, const unsigned int sampling=10, const float factor=-15);
	CImg<double> & get_arrowImg(CImg<double> &u, CImg<double> &v, int r=0, int g=0, int b=0, const float opacity=1, const unsigned int sampling=10, const float factor=-15);
	CImg<double> & get_arrowImg(OpticalFlow &OF, int r=0, int g=0, int b=0, const float opacity=1, const unsigned int sampling=10, const float factor=-15);

	void arrowImg(CImg<double> &u, CImg<double> &v, CImg<double> &imfond, int r=0, int g=0, int b=0, const float opacity=1, const unsigned int sampling=10, const float factor=-15);
	CImg<double> & get_arrowImg(CImg<double> &u, CImg<double> &v, CImg<double> &imfond, int r=0, int g=0, int b=0, const float opacity=1, const unsigned int sampling=10, const float factor=-15);
	CImg<double> & get_arrowImg(OpticalFlow &OF, CImg<double> &imfond, int r=0, int g=0, int b=0, const float opacity=1, const unsigned int sampling=10, const float factor=-15);

	//magnitude
	void magnitudeImg(CImg<double> &u, CImg<double> &v, int min=0, int max=0);
	CImg<double> & get_magnitudeImg(CImg<double> &u, CImg<double> &v, int min=0, int max=0);
};

#endif /* DisplayOF_H_ */

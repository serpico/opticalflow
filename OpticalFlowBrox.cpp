#include "OpticalFlowBrox.h"
#include "ImgTools.h"
#include <cstdlib>
#include <iostream>
#include <math.h>
#include "DisplayOF.h"
#include <stdio.h>

using namespace std;
using namespace cimg_library;

#define OCCLUSION 1666666752

OpticalFlowBrox::OpticalFlowBrox(const OpticalFlowBrox &brox_OF)
{
	set_parameters(brox_OF.get_coeff_reg(), brox_OF.get_coeff_grad(), brox_OF.get_pyramid_levels(),
			brox_OF.get_pyramid_ratio(), brox_OF.get_res_iter(), brox_OF.get_outer_iter(), brox_OF.get_eps(), brox_OF.get_median_size(),
			brox_OF.get_min_size_pyramid(), brox_OF.get_norm(), brox_OF.get_texture(), brox_OF.get_sigma(), brox_OF.get_midway());
	set_imgs(brox_OF.get_I1(), brox_OF.get_I2());
	set_flow(brox_OF.get_u(), brox_OF.get_v());
}

OpticalFlowBrox::OpticalFlowBrox	(double coeff_reg, double coeff_grad, int pyramid_levels, double pyramid_ratio, int res_iter, int outer_iter, double eps, int median_size, double min_size_pyramid, bool norm, bool texture, double sigma, int midway) : OpticalFlow()
{
	set_parameters(coeff_reg, coeff_grad, pyramid_levels, pyramid_ratio, res_iter, outer_iter,eps, median_size,
			min_size_pyramid, norm, texture, sigma, midway);
}

OpticalFlowBrox::OpticalFlowBrox	(CImg<double> &I1, CImg<double> &I2, double coeff_reg, double coeff_grad,
									int pyramid_levels, double pyramid_ratio, int res_iter, int outer_iter, double eps, int median_size,
									double min_size_pyramid, bool norm, bool texture, double sigma, int midway) : OpticalFlow(I1, I2)
{
	set_parameters(coeff_reg, coeff_grad, pyramid_levels, pyramid_ratio, res_iter, outer_iter,eps, median_size,
			min_size_pyramid, norm, texture, sigma, midway);
}

OpticalFlowBrox::~OpticalFlowBrox()
{
}

void OpticalFlowBrox::set_parameters	(double coeff_reg, double coeff_grad, int pyramid_levels, double pyramid_ratio,
										int res_iter, int outer_iter, double eps, int median_size, double min_size_pyramid, bool norm, bool texture,
										double sigma, int midway)
{
	this->coeff_reg = coeff_reg;
	this->coeff_grad = coeff_grad;
	this->pyramid_levels = pyramid_levels;
	this->pyramid_ratio = pyramid_ratio;
	this->res_iter = res_iter;
	this->outer_iter = outer_iter;
	this->eps = eps;
	this->median_size = median_size;
	this->min_size_pyramid = min_size_pyramid;
	this->norm = norm;
	this->texture = texture;
	this->sigma = sigma;
	this->midway = midway;
}

void OpticalFlowBrox::set_coeff_reg(double coeff_reg)
{
	this->coeff_reg = coeff_reg;
}

void OpticalFlowBrox::set_min_size_pyramid(double min_size_pyramid)
{
	this->min_size_pyramid = min_size_pyramid;
}

void OpticalFlowBrox::set_pyramid_levels(double pyramid_levels)
{
	this->pyramid_levels = pyramid_levels;
}

bool OpticalFlowBrox::get_norm() const
{
	return norm;
}

double OpticalFlowBrox::get_coeff_reg() const
{
	return coeff_reg;
}
double OpticalFlowBrox::get_coeff_grad() const
{
	return coeff_grad;
}
int OpticalFlowBrox::get_pyramid_levels() const
{
	return pyramid_levels;
}
double OpticalFlowBrox::get_pyramid_ratio() const
{
	return pyramid_ratio;
}
double OpticalFlowBrox::get_eps() const
{
	return eps;
}
int OpticalFlowBrox::get_res_iter() const
{
	return res_iter;
}
int OpticalFlowBrox::get_outer_iter() const
{
	return outer_iter;
}
int OpticalFlowBrox::get_median_size() const
{
	return median_size;
}
double OpticalFlowBrox::get_min_size_pyramid() const
{
	return min_size_pyramid;
}
bool OpticalFlowBrox::get_texture() const
{
	return texture;
}

bool OpticalFlowBrox::get_midway() const
{
	return midway;
}
int OpticalFlowBrox::get_sigma() const
{
	return sigma;
}

void OpticalFlowBrox::compute_OF()
{
	// Structure/texture decomposition
	if(texture == true)
	{
		CImg<double> structure, texture;
		ImgTools::structure_texture_rof(structure, texture, I1); I1 = texture;
		ImgTools::structure_texture_rof(structure, texture, I2); I2 = texture;

		CImgDisplay disp(I1);
		while (!disp.is_keyENTER())
			disp.wait();
		CImgDisplay disp2(I2);
		while (!disp2.is_keyENTER())
			disp2.wait();

	}

	// Adaptation of pyramid_levels
	if(min_size_pyramid != 0)
	{
		int size = min(I1.width(), I1.height());
		int real_min_size_pyramid = floor((double)size/min_size_pyramid);
		int level = 0;
		while(pyramid_ratio*size > real_min_size_pyramid)
		{
			size = floor(pyramid_ratio*size);
			level++;
		}
		pyramid_levels = level;
		if(pyramid_levels == 0)
		{
			cout << "min_pyramid_size > image size !!!" << endl;
			exit(0);
		}
	}

	// Construct image pyramid
	vector < vector< CImg<double> > > pyramid(2);
	pyramid[0].push_back(I1);
	pyramid[1].push_back(I2);
	CImg<double> Itmp1(I1), Itmp2(I2), Ixtmp2, Iytmp2;
	double sum_grad;int nb_pixs;
	for(int i=0; i<pyramid_levels-1; i++)
	{
		Itmp1 = ImgTools::gaussian_rescaling(I1,pow(pyramid_ratio, i+1));
		Itmp2 = ImgTools::gaussian_rescaling(I2,pow(pyramid_ratio, i+1));
		pyramid[0].push_back(Itmp1);
		pyramid[1].push_back(Itmp2);
	}

	// Initialisation with the nul motion field
	u.assign(pyramid[0][pyramid_levels-1].width(), pyramid[0][pyramid_levels-1].height(), 1, 1, 0); v.assign(u);
	CImg<double> coeff_reg_img(I1.width(), I1.height(),1,1,coeff_reg);
	CImg<double> du, dv, I1_level, I2_level, mask;
	int height, width;

	for(int i=pyramid_levels-1; i>=0; i--)
	{cout << i << endl;
		I1_level = pyramid[0][i];
		I2_level = pyramid[1][i];

		height = pyramid[0][i].height();
		width = pyramid[0][i].width();

		// Resize optical flow
		u.resize(width, height, 1, 1, 2);
		v.resize(width, height, 1, 1, 2);

		// Warp image 2
		I2_level = ImgTools::get_warp_ipol(I2_level, u, v,mask, "bicubic");

		coeff_reg_img.assign(width, height, 1, 1, coeff_reg);

		// Optical flow estimation for one scale (with linearization)
		resolutionProcess_brox(u, v, I1_level, I2_level, u, v, coeff_reg_img, mask);

		// Median filtering
		if(median_size != 0)
		{
			u.blur_median(median_size);
			v.blur_median(median_size);
		}
	}
}

void OpticalFlowBrox::resolutionProcess_brox(CImg<double> &u_new, CImg<double> &v_new, CImg<double> &I1, CImg<double> &I2, CImg<double> &u_prev, CImg<double> &v_prev, CImg<double> &coeff_reg_img, CImg<double> &mask)//, CImg<double> &Ikz, CImg<double> &Ikx, CImg<double> &Iky, CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &coeff_reg_img, double coeff_grad, double omega, int outer_iter, int res_iter, double eps)
{
	int width = I1.width();
	int height = I1.height();

	CImg<double> u0 = u, v0 = v, I20=I2;

	double omega = 1.9; // SOR parameter

	// Initialize du, dv to 0
	// duv is the single vector which will hold the solution of SOR computations
	CImg<double> du(width, height, 1, 1, 0), dv(du);
	CImg<double> duv(2 * width * height, 1, 1, 1, 0);
	u_new = u_prev; v_new = v_prev;

	CImg<double> psidash, psidash2, psidashFS;
	CImg<double> tmp1, tmp2, tmp3;
	CImg<double> rs, cs ,ss, b;

	double error, tol = 1e-8;
	int nbiters, x, y;
	bool flag = 0;
	CImg<double> mask2;

	// Outer_iter iterations
	for(int i = 0; i<outer_iter; i++)
	{
		I2 = ImgTools::get_warp_ipol(I2, du, dv,mask2, "bicubic");

		// Compute first order derivatives
		CImg<double> Ix, Iy, Ikx, Iky, Ikx2, Iky2, Iz, Ixz, Iyz;
		ImgTools::get_temporal_derivative(I1, I2, Ix, Iy);
		ImgTools::get_derivative(I2, Ikx, Iky);
		ImgTools::get_derivative(I1, Ikx2, Iky2);

		Iz = I2 - I1;
		Ixz = Ikx - Ikx2;
		Iyz = Iky - Iky2;

		// Compute second order derivatives
		CImg<double> Ixx, Ixy, Iyy, Iyx;
		ImgTools::get_temporal_derivative(Ikx, Ikx2, Ixx, Ixy);
		ImgTools::get_temporal_derivative(Iky, Iky2, Iyx, Iyy);

		// Construct the new matrix
		constructMatrix(rs, cs, ss, b, Ix, Iy, Iz, Ixx, Ixy, Iyx, Iyy, Ixz, Iyz, u_new, v_new, du, dv, mask);

		// Call SOR
		sor(duv, error, nbiters, flag, rs, cs, ss, b, omega, res_iter, tol);

		// Convert duv into du and dv
		int size_dudv_div2 = duv.width()/2;
		for(int i=0; i<size_dudv_div2; i++)
			du(i%width,floor(i/width)) = duv(i);
		for(int i=size_dudv_div2; i<size_dudv_div2*2; i++)
			dv((i-size_dudv_div2)%width,floor((i-size_dudv_div2)/width)) = duv(i);

		if(median_size != 0)
		{
			du.blur_median(median_size);
			dv.blur_median(median_size);
		}

		u_new += du;
		v_new += dv;
	}
}

inline double OpticalFlowBrox::psi_value(double x, double eps)
{
	return sqrt(x + eps*eps);
}

inline double OpticalFlowBrox::psiDerivative_value(double x, double eps)
{
	return 1 / (2 * sqrt(x + eps*eps));
}

inline double OpticalFlowBrox::psiDerivative2_value(double x, double eps)
{
	return 1 / (2 * sqrt(x + eps*eps));
}

void OpticalFlowBrox::compute_psidash_D1(CImg<double> &psidash_D1, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &u, CImg<double> &v, CImg<double> &normI, bool normalization)
{
	psidash_D1.assign(Ix.width(), Ix.height(), 1, Ix.spectrum());
	if(normalization == 0)
		cimg_forXYC(psidash_D1,x,y,c)
			psidash_D1(x,y,0,c) = psiDerivative_value(pow(Iz(x,y,0,c) + Ix(x,y,0,c)*u(x,y) + Iy(x,y,0,c)*v(x,y), 2));

	if(normalization == 1)
		cimg_forXYC(psidash_D1,x,y,c)
			psidash_D1(x,y,0,c) = psiDerivative_value(normI(x,y) * pow(Iz(x,y,0,c) + Ix(x,y,0,c)*u(x,y) + Iy(x,y,0,c)*v(x,y), 2));
}

void OpticalFlowBrox::compute_psidash_D2(CImg<double> &psidash_D2, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyy,  CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &normIx, CImg<double> &normIy, bool normalization)
{
	psidash_D2.assign(Ixx.width(), Ixx.height(), 1, Ixx.spectrum());
	if(normalization == 0)
		cimg_forXYC(psidash_D2,x,y,c)
			psidash_D2(x,y,0,c) = psiDerivative_value(pow(Ixz(x,y,0,c) + Ixx(x,y,0,c)*u(x,y) + Ixy(x,y,0,c)*v(x,y), 2) + pow(Iyz(x,y,0,c) + Ixy(x,y,0,c)*u(x,y) + Iyy(x,y,0,c)*v(x,y), 2));

	if(normalization == 1)
		cimg_forXYC(psidash_D2,x,y,c)
			psidash_D2(x,y,0,c) = psiDerivative_value(normIx(x,y) * pow(Ixz(x,y,0,c) + Ixx(x,y,0,c)*u(x,y) + Ixy(x,y,0,c)*v(x,y), 2) + normIy(x,y) * pow(Iyz(x,y,0,c) + Ixy(x,y,0,c)*u(x,y) + Iyy(x,y,0,c)*v(x,y), 2));

}

void OpticalFlowBrox::compute_psidash2_D1(CImg<double> &psidash_D1, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &u, CImg<double> &v, CImg<double> &normI, bool normalization)
{
	psidash_D1.assign(Ix.width(), Ix.height(), 1, Ix.spectrum());
	if(normalization == 0)
		cimg_forXYC(psidash_D1,x,y,c)
			psidash_D1(x,y,0,c) = psiDerivative2_value(pow(Iz(x,y,0,c) + Ix(x,y,0,c)*u(x,y) + Iy(x,y,0,c)*v(x,y), 2));

	if(normalization == 1)
		cimg_forXYC(psidash_D1,x,y,c)
			psidash_D1(x,y,0,c) = psiDerivative2_value(normI(x,y) * pow(Iz(x,y,0,c) + Ix(x,y,0,c)*u(x,y) + Iy(x,y,0,c)*v(x,y), 2));
}

void OpticalFlowBrox::compute_psidash2_D2(CImg<double> &psidash_D2, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyy,  CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &normIx, CImg<double> &normIy, bool normalization)
{
	psidash_D2.assign(Ixx.width(), Ixx.height(), 1, Ixx.spectrum());
	if(normalization == 0)
		cimg_forXYC(psidash_D2,x,y,c)
			psidash_D2(x,y,0,c) = psiDerivative2_value(pow(Ixz(x,y,0,c) + Ixx(x,y,0,c)*u(x,y) + Ixy(x,y,0,c)*v(x,y), 2) + pow(Iyz(x,y,0,c) + Ixy(x,y,0,c)*u(x,y) + Iyy(x,y,0,c)*v(x,y), 2));

	if(normalization == 1)
		cimg_forXYC(psidash_D2,x,y,c)
			psidash_D2(x,y,0,c) = psiDerivative2_value(normIx(x,y) * pow(Ixz(x,y,0,c) + Ixx(x,y,0,c)*u(x,y) + Ixy(x,y,0,c)*v(x,y), 2) + normIy(x,y) * pow(Iyz(x,y,0,c) + Ixy(x,y,0,c)*u(x,y) + Iyy(x,y,0,c)*v(x,y), 2));

}

void OpticalFlowBrox::compute_psidash_S(CImg<double> &psidash_S, CImg<double> &u, CImg<double> &v)
{
	CImg<double> ux,uy,vx,vy;
	ImgTools::get_derivative(u,ux,uy,2);
	ImgTools::get_derivative(v,vx,vy,2);

	CImg<double> psidash_S0(u.width(), u.height(), 1, 1, 0);
	//int x, y;
	cimg_forXY(psidash_S0,x,y)
		psidash_S0(x,y) = psiDerivative_value(ux(x,y)*ux(x,y) + uy(x,y)*uy(x,y) + vx(x,y)*vx(x,y) + vy(x,y)*vy(x,y));

	ImgTools::extend(psidash_S0,psidash_S);
}

void OpticalFlowBrox::compute_psidash(CImg<double> &psidash_D1, CImg<double> &psidash_D2, CImg<double> &psidash_S, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyy,  CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v,  CImg<double> &du, CImg<double> &dv, CImg<double> &normI, CImg<double> &normIx, CImg<double> &normIy, bool normalization)
{
	compute_psidash_D1(psidash_D1,Ix,Iy,Iz,du,dv, normI, normalization);
	compute_psidash_D2(psidash_D2,Ixx,Ixy,Iyy,Ixz,Iyz,du,dv, normIx, normIy, normalization);
	compute_psidash_S(psidash_S,u,v);
}

void OpticalFlowBrox::constructMatrix(CImg<double> &row_out, CImg<double> &col_out, CImg<double> &val_out, CImg<double> &b, CImg<double> &Ix, CImg<double> &Iy, CImg<double> &Iz, CImg<double> &Ixx, CImg<double> &Ixy, CImg<double> &Iyx, CImg<double> &Iyy, CImg<double> &Ixz, CImg<double> &Iyz, CImg<double> &u, CImg<double> &v, CImg<double> &du, CImg<double> &dv, CImg<double> &mask)
{
	int width = u.width();
	int height = u.height();
	int nb_pixs = width*height;

	CImg<double> coeff_reg_img(width, height, 1, 1, coeff_reg);
	CImg<double> coeff_reg_vect = coeff_reg_img.get_vector();

	// Variables of the sparse matrix A
	CImg<double> row(2*nb_pixs*6, 1, 1, 1, 0), col(row), val(row);

	// Elements of the matrix A
	CImg<double> J11(width, height, 1, 1, 0), J12(J11), J21(J11), J22(J11), div_ij(J11), div_ip(J11), div_im(J11), div_jp(J11), div_jm(J11);
	CImg<double> psidash_D1, psidash_D2, psidash_S;
	CImg<double> normI(width, height, 1, 1, 1), normIx(normI), normIy(normI); // Normalization coeffs for the brightness and gradient terms
	if(norm == 1)
	{
		cimg_forXY(normI,x,y)
		{
			normI(x,y) = 1 / (Ix(x,y)*Ix(x,y) + Iy(x,y)*Iy(x,y) + 0.1);
			normIx(x,y) = 1 / (Ixx(x,y)*Ixx(x,y) + Ixy(x,y)*Ixy(x,y) + 0.1);
			normIy(x,y) = 1 / (Ixy(x,y)*Ixy(x,y) + Iyy(x,y)*Iyy(x,y) + 0.1);
		}
	}
	if(norm == 0)
	{
		cimg_forXY(normI,x,y)
		{
			normI(x,y) = 1;
			normIx(x,y) = 1;
			normIy(x,y) = 1;
		}
	}
	compute_psidash(psidash_D1,psidash_D2,psidash_S,Ix,Iy,Iz,Ixx,Ixy,Iyy,Ixz,Iyz,u,v,du,dv,normI,normIx,normIy,1);

	int x_S, y_S;
	cimg_forXY(J11,x,y)
	{
		cimg_forC(I1,c)
		{
			J11(x,y) += mask(x,y) * (normI(x,y) * psidash_D1(x,y,0,c) * Ix(x,y,0,c)*Ix(x,y,0,c) + coeff_grad * psidash_D2(x,y,0,c) * (normIx(x,y) * Ixx(x,y,0,c)*Ixx(x,y,0,c) + normIy(x,y) * Iyx(x,y,0,c)*Iyx(x,y,0,c)));
			J12(x,y) += mask(x,y) * (normI(x,y) * psidash_D1(x,y,0,c) * Ix(x,y,0,c)*Iy(x,y,0,c) + coeff_grad * psidash_D2(x,y,0,c) * (normIx(x,y) * Ixx(x,y,0,c)*Ixy(x,y,0,c) + normIy(x,y) * Ixy(x,y,0,c)*Iyy(x,y,0,c)));
			J21(x,y) += mask(x,y) * (normI(x,y) * psidash_D1(x,y,0,c) * Ix(x,y,0,c)*Iy(x,y,0,c) + coeff_grad * psidash_D2(x,y,0,c) * (normIx(x,y) * Ixx(x,y,0,c)*Ixy(x,y,0,c) + normIy(x,y) * Iyx(x,y,0,c)*Iyy(x,y,0,c)));
			J22(x,y) += mask(x,y) * (normI(x,y) * psidash_D1(x,y,0,c) * Iy(x,y,0,c)*Iy(x,y,0,c) + coeff_grad * psidash_D2(x,y,0,c) * (normIx(x,y) * Ixy(x,y,0,c)*Ixy(x,y,0,c) + normIy(x,y) * Iyy(x,y,0,c)*Iyy(x,y,0,c)));
		}
		x_S = x+1; y_S = y+1;
		div_ij(x,y) = -0.125*(4*psidash_S(x_S,y_S) + psidash_S(x_S+1,y_S) + psidash_S(x_S-1,y_S) + psidash_S(x_S,y_S+1) + psidash_S(x_S,y_S-1));
		div_ip(x,y) = 0.125*(psidash_S(x_S+1,y_S) + psidash_S(x_S,y_S));
		div_im(x,y) = 0.125*(psidash_S(x_S,y_S) + psidash_S(x_S-1,y_S));
		div_jp(x,y) = 0.125*(psidash_S(x_S,y_S+1) + psidash_S(x_S,y_S));
		div_jm(x,y) = 0.125*(psidash_S(x_S,y_S) + psidash_S(x_S,y_S-1));
	}
	// Neuman boundary conditions
	cimg_forX(div_ij,x)
	{
		div_ij(x,0) += div_jm(x,0);
		div_jm(x,0) = 0;
		div_ij(x,height-1) += div_jp(x,height-1);
		div_jp(x,height-1) = 0;
	}
	cimg_forY(div_ij,y)
	{
		div_ij(0,y) += div_im(0,y);
		div_im(0,y) = 0;
		div_ij(width-1,y) += div_ip(width-1,y);
		div_ip(width-1,y) = 0;
	}

	CImg<double> w(width, height,1,1,0);

	// Put matrices into vectors
	CImg<double> J11_vect = J11.get_vector(), J12_vect = J12.get_vector(), J21_vect = J21.get_vector(), J22_vect = J22.get_vector();
	CImg<double> div_ij_vect = div_ij.get_vector(), div_ip_vect = div_ip.get_vector(), div_im_vect = div_im.get_vector(), div_jp_vect = div_jp.get_vector(), div_jm_vect = div_jm.get_vector();
	CImg<double> w_vect = w.get_vector();
	// Fill the sparse matrix A
	int i_vect = 0;

	// Arguments to u
	for(int i=0; i<nb_pixs; i++)
	{
		row(i_vect) = i; col(i_vect) = i-width; val(i_vect) = - coeff_reg_vect(i) * div_jm_vect(i);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i-1; val(i_vect) = - coeff_reg_vect(i) * div_im_vect(i);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i; val(i_vect) = J11_vect(i) - coeff_reg_vect(i) * div_ij_vect(i);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i+1; val(i_vect) = - coeff_reg_vect(i) * div_ip_vect(i);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i+width; val(i_vect) = - coeff_reg_vect(i) * div_jp_vect(i);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i + nb_pixs; val(i_vect) = J12_vect(i);
		i_vect++;
	}

	// Arguments to v
	int itmp;
	for(int i=nb_pixs; i<2*nb_pixs; i++)
	{
		itmp = i - nb_pixs;
		row(i_vect) = i; col(i_vect) = i - nb_pixs; val(i_vect) = J21_vect(itmp);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i-width; val(i_vect) = - coeff_reg_vect(itmp) * div_jm_vect(itmp);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i-1; val(i_vect) = - coeff_reg_vect(itmp) * div_im_vect(itmp);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i; val(i_vect) = J22_vect(itmp) - coeff_reg_vect(itmp) * div_ij_vect(itmp);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i+1; val(i_vect) = - coeff_reg_vect(itmp) * div_ip_vect(itmp);
		i_vect++;
		row(i_vect) = i; col(i_vect) = i+width; val(i_vect) = - coeff_reg_vect(itmp) * div_jp_vect(itmp);
		i_vect++;
	}

	// Elements of b
	CImg<double> u_ex, v_ex; int x_ex, y_ex;
	ImgTools::extend(u, u_ex,1); ImgTools::extend(v, v_ex,1);
	CImg<double> J13(width, height, 1, 1, 0), J23(J13), div_u(J13), div_v(J13), g_u(J13), g_v(J13);
	cimg_forXY(J13,x,y)
	{
		cimg_forC(I1,c)
		{
			J13(x,y) += mask(x,y) * (normI(x,y) * psidash_D1(x,y,0,c) * Ix(x,y,0,c)*Iz(x,y,0,c) + coeff_grad * psidash_D2(x,y,0,c) * (normIx(x,y) * Ixx(x,y,0,c)*Ixz(x,y,0,c) + normIy(x,y) * Iyx(x,y,0,c)*Iyz(x,y,0,c)));
			J23(x,y) += mask(x,y) * (normI(x,y) * psidash_D1(x,y,0,c) * Iy(x,y,0,c)*Iz(x,y,0,c) + coeff_grad * psidash_D2(x,y,0,c) * (normIx(x,y) * Ixy(x,y,0,c)*Ixz(x,y,0,c) + normIy(x,y) * Iyy(x,y,0,c)*Iyz(x,y,0,c)));
		}
		x_ex = x+1; y_ex = y+1;
		div_u(x,y) = div_ij(x,y)*u_ex(x_ex,y_ex) + div_ip(x,y)*u_ex(x_ex+1,y_ex) + div_im(x,y)*u_ex(x_ex-1,y_ex) + div_jp(x,y)*u_ex(x_ex,y_ex+1) + div_jm(x,y)*u_ex(x_ex,y_ex-1);
		div_v(x,y) = div_ij(x,y)*v_ex(x_ex,y_ex) + div_ip(x,y)*v_ex(x_ex+1,y_ex) + div_im(x,y)*v_ex(x_ex-1,y_ex) + div_jp(x,y)*v_ex(x_ex,y_ex+1) + div_jm(x,y)*v_ex(x_ex,y_ex-1);
	}
	// Put matrices into vectors
	CImg<double> J13_vect = J13.get_vector(), J23_vect = J23.get_vector();
	CImg<double> div_u_vect = div_u.get_vector(), div_v_vect = div_v.get_vector();
	CImg<double> g_u_vect = g_u.get_vector(), g_v_vect = g_v.get_vector();

	// Fill b
	b.assign(2*nb_pixs,1,1,1,0);
	// Arguments to u
	for(int i=0; i<nb_pixs; i++)
		b(i) = -J13_vect(i) + coeff_reg_vect(i) * div_u_vect(i);

	// Arguments to v
	for(int i=nb_pixs; i<2*nb_pixs; i++)
	{
		itmp = i - nb_pixs;
		b(i) = -J23_vect(itmp) + coeff_reg_vect(itmp) * div_v_vect(itmp);
	}

	// Trim
	int i = 0;
	cimg_forX(col,x)
	{
		if(row(x) < nb_pixs)
			if((col(x) >= 0 && col(x) < nb_pixs) || col(x) == row(x) + nb_pixs)
				i++;

		if(row(x) >= nb_pixs)
			if((col(x) >= nb_pixs && col(x) < 2*nb_pixs)  || col(x) == row(x) - nb_pixs)
				i++;
	}
	col_out.assign(i,1,1,1,0); row_out.assign(col_out); val_out.assign(col_out);
	i = 0;
	cimg_forX(col,x)
	{
		if(row(x) < nb_pixs)
			if((col(x) >= 0 && col(x) < nb_pixs) || col(x) == row(x) + nb_pixs)
			{
				col_out(i) = col(x);
				row_out(i) = row(x);
				val_out(i) = val(x);
				i++;
			}
		if(row(x) >= nb_pixs)
			if((col(x) >= nb_pixs && col(x) < 2*nb_pixs)  || col(x) == row(x) - nb_pixs)
			{
				col_out(i) = col(x);
				row_out(i) = row(x);
				val_out(i) = val(x);
				i++;
			}
	}
}

void OpticalFlowBrox::sor(CImg<double> &duv, double error, int iter, bool flag, CImg<double> &rs, CImg<double> &cs, CImg<double> &ss, CImg<double> &b, double w, int max_it, double tol)
{
	int ii;

	duv.assign(b.width(), b.height(), 1, 1, 0);
	CImg<double> dudv_prev;

	double sum = 0;

	for(int it=0; it<max_it; it++)
	{
		int irs = 0;
		dudv_prev = duv;
		cimg_forX(duv,x)
		{
			sum = 0;
			while(rs(irs) == x)
			{
				if(cs(irs) < rs(irs))
					sum += ss(irs) * duv(cs(irs));
				else if(cs(irs) > rs(irs))
					sum += ss(irs) * dudv_prev(cs(irs));
				else
					ii = irs;

				irs++;
			}
			duv(x) = (1-w) * dudv_prev(x) + w/ss(ii) * (b(x) - sum);
		}
	}
}

void OpticalFlowBrox::write_error_header(string &error_filename, string &I1_filename, string &I2_filename)
{
	ofstream error_file(error_filename.c_str());
	error_file << "# Brox method" << endl;
	error_file << endl;

	error_file << "# Images" << endl;
	error_file << I1_filename << endl;
	error_file << I2_filename << endl;
	error_file << endl;

	error_file << "# Parameters" << endl;
	error_file << "coeff_reg = " << coeff_reg << endl;
	error_file << "coeff_grad = " << coeff_grad << endl;
	error_file << "pyramid_levels = " << pyramid_levels << endl;
	error_file << "pyramid_ratio = " << pyramid_ratio << endl;
	error_file << "eps = " << eps  << endl;
	error_file << "res_iter = " << res_iter << endl;
	error_file << "outer_iter = " << outer_iter << endl;
	error_file << "median_size = " << median_size << endl;
	error_file << "min_size_pyramid = " << min_size_pyramid << endl;
	error_file << "texture = " << texture << endl;
	error_file << "sigma = " << sigma << endl;
	error_file << endl;
}

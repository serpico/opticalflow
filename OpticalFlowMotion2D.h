
#ifndef _OpticalFlowMotion2D_h
#define _OpticalFlowMotion2D_h

#include "CImg.h"
#include <CMotion2DPyramid.h>
#include <iostream>
#include "OpticalFlow.h"
#include <CMotion2DEstimator.h>

using namespace cimg_library;
using namespace std;

class OpticalFlowMotion2D : public OpticalFlow
{
public:
	OpticalFlowMotion2D(CImg<double> &I1, CImg<double> &I2);
	~OpticalFlowMotion2D();

	void compute_OF();

	void set_parameters(string model_id="AC", bool var_light=false, bool model_orig_fixed=false, double model_row_orig=-1, double model_col_orig=-1, unsigned pyr_nlevels=4, bool compute_covariance=false, const char *robust="tukey");

	double* get_coeffs();

private:

	  string model_id;	// Parametric motion model ID to estimate; default 'AC'
	  bool var_light;	// Lighting variation parameter estimation; default false
	  bool model_orig_fixed;// Indicates if an origin is fixed; default false
	  double model_row_orig;  // Motion model origin (line coordinate); default -1.
	  double model_col_orig;  // Motion model origin (column coordinate); default -1.
	  unsigned pyr_nlevels;	// Number of levels in a multi-resolution pyr; defaut 4
	  bool compute_covariance; // Flag to compute the covariance matrix; default false
	  double coeffs[16];
	  const char *robust;
};

#endif


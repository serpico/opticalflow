#include "ImgTools.h"
#include <algorithm>
#include <assert.h>

using namespace cimg_library;
using namespace std;

const double pi = 3.14159265358979323846;

#define OCCLUSION 1666666752
#define BOUNDARY_CONDITION 0

CImg<double> ImgTools::gaussian_rescaling(CImg<double> &I, double scale_factor)
{
	if(scale_factor<0.05)
	{
		cout << "Too small scaling factor!!" << endl;
	}
	else
	{
		CImg<double> output(I.width()*scale_factor, I.height()*scale_factor,1,I.spectrum());
		CImg<double> I_channel(I.width(), I.height(), 1, 1);
		CImg<double> img_blur;
		cimg_forC(I,c)
		{
			cimg_forXY(I,x,y)
				I_channel(x,y) = I(x,y,0,c);

			img_blur = I_channel.get_blur(sqrt(2)/(4*scale_factor));//1/sqrt(2*scale_factor));
			img_blur.resize(I_channel.width()*scale_factor, I_channel.height()*scale_factor,1,1,2);

			cimg_forXY(img_blur,x,y)
				output(x,y,0,c) = img_blur(x,y);
		}
		return output;
	}
}

CImg<double> ImgTools::get_rgb2gray(const CImg<double> &I)
{
	if(I.spectrum() == 1)		return I;
	else
	{
		CImg<double> output(I.width(), I.height(),1,1);
		cimg_forXY(I,x,y)
		{
			output(x,y) = 0.2989 * I(x,y,0) + 0.587 * I(x,y,1) + 0.114 * I(x,y,2);
		}
		return output;
	}
}

void ImgTools::rgb2gray(CImg<double> &I)
{
	I = get_rgb2gray(I);
}

void ImgTools::get_derivative(CImg<double> &I, CImg<double> &Ix, CImg<double> &Iy, int filter)
{
	Ix.assign(I.width(), I.height(), I.depth(), I.spectrum()); Iy.assign(Ix);
	CImg<double> I_channel(I.width(), I.height(), 1, 1), Ix_channel(I_channel), Iy_channel(I_channel);
	cimg_forC(I,c)
	{
		cimg_forXY(I,x,y)
			I_channel(x,y) = I(x,y,0,c);

		if(filter == 1)
		{
			CImg<double> gradfilter(5, 1, 1, 1,   -1,8,0,-8,1);
			gradfilter /= 12;
			Ix_channel = I_channel.get_convolve(gradfilter);
			Iy_channel = I_channel.get_convolve(gradfilter.get_transpose());
		}
		if(filter == 2)
		{
			CImg<double> gradfilter(3, 1, 1, 1,   -1,0,1);
			gradfilter /= 2;
			Ix_channel = I_channel.get_convolve(gradfilter);
			Iy_channel = I_channel.get_convolve(gradfilter.get_transpose());
		}

		cimg_forXY(I,x,y)
		{
			Ix(x,y,0,c) = Ix_channel(x,y);
			Iy(x,y,0,c) = Iy_channel(x,y);
		}
	}
//	Ix = I.get_gradient()[0];
//	Iy = I.get_gradient()[1];

}


void ImgTools::get_temporal_derivative(CImg<double> &I1, CImg<double> &I2, CImg<double> &Ix, CImg<double> &Iy, double blend)
{
	CImg<double> gradfilter(5, 1, 1, 1,   -1,8,0,-8,1);
	gradfilter /= 12;

	Ix = I2.get_convolve(gradfilter);
	Iy = I2.get_convolve(gradfilter.get_transpose());

	CImg<double> Ix1 = I1.get_convolve(gradfilter);
	CImg<double> Iy1 = I1.get_convolve(gradfilter.get_transpose());

	Ix = blend * Ix + (1 - blend) * Ix1;
	Iy = blend * Iy + (1 - blend) * Iy1;
}

void get_pix_derivative(CImg<double> &I, double dx, double dy)
{
	CImg<double> gradfilter(5, 1, 1, 1,   -1,8,0,-8,1);

}

CImg<double> ImgTools::grad_magnitude(CImg<double> &I)
{
	CImg<double> Ix, Iy;
	get_derivative(I, Ix, Iy);
	return (Ix.pow(2) + Iy.pow(2)).sqrt();
}

void ImgTools::warp(CImg<double>& src, CImg<double>& u, CImg<double>& v, string interp_method)
{
	src = get_warp(src, u, v, interp_method);
}

CImg<double> ImgTools::get_warp(CImg<double>& src, CImg<double>& u, CImg<double>& v, string interp_method)
{
	CImg<double> warped(src);

	if(interp_method.compare("bilinear") == 0)
		cimg_forXYC(warped,x,y,c)
			warped(x,y,0,c) = src.linear_atXY(x + u(x,y), y + v(x,y),0,c);

	else if(interp_method.compare("bicubic") == 0)
		cimg_forXYC(warped,x,y,c)
			warped(x,y,0,c) = src.cubic_atXY(x + u(x,y), y + v(x,y),0,c);

	else
		cout << "unknown interpolation method" << endl;

	return warped;
}

double ImgTools::get_variance(CImg<double> &I)
{
	double variance, mean = I.mean(), sum = 0;
	int nbPixs = 0;
	cimg_forXY(I,x,y)
		if(I(x,y) != -1)
		{
			sum += pow(I(x,y) - mean,2);
			nbPixs++;
		}

	return sum/(nbPixs-1);
}

void ImgTools::compute_eigenValues(CImg<double> &tensor, double &lambda1, double &lambda2, double &lambda3)
{
	CImg<double> tmp_eigenValues, tmp_eigenVectors;

	tensor.symmetric_eigen(tmp_eigenValues, tmp_eigenVectors);

	if(tensor.width() == 2)
	{
		lambda1 = tmp_eigenValues(0,0);
		lambda2 = tmp_eigenValues(0,1);
		lambda3 = -8888;
	}
	else
	{
		lambda1 = tmp_eigenValues(0,0);
		lambda2 = tmp_eigenValues(0,1);
		lambda3 = tmp_eigenValues(0,2);
	}
}

double ImgTools::scalar_prod(CImg<double> &v1, CImg<double> &v2)
{
	double scalar = 0;
	cimg_forX(v1,x)
		scalar += v1(x) * v2(x);
	return scalar;
}

string ImgTools::imgName(const char *filename)
{
	string output;
	string str_filename(filename);
	int nbegin = str_filename.find_last_of("/");
	int nend = str_filename.find(".", nbegin);
	for(int i=nbegin+1; i<nend-1; i++)
		output += str_filename.at(i);
	return output;
}

CImg<double> ImgTools::get_rescale(const CImg<double> &I, double min, double max)
{
	double old_min = I.min();
	double old_max = I.max();

	return (I - old_min) / (old_max - old_min) * (max - min) + min;
}

void ImgTools::rescale(CImg<double> &I, double min, double max)
{
	I = get_rescale(I,min,max);
}

void ImgTools::rescale_2frames(CImg<double> &I1, CImg<double> &I2, double min_rescale, double max_rescale)
{
	double Imax = max(I1.max(), I2.max());
	double Imin = min(I1.min(), I2.min());

	I1 = (I1 -Imin) / (Imax - Imin) * (max_rescale - min_rescale) + min_rescale;
	I2 = (I2 -Imin) / (Imax - Imin) * (max_rescale - min_rescale) + min_rescale;
}

void ImgTools::rescale_tif(CImg<double> &I, double min, double max)
{
	double Imax = 0, Imin = 100000;
	cimg_forXYZ(I,x,y,z)
	{
		if(I(x,y,z) > Imax)
			Imax = I(x,y,z);
		if(I(x,y,z) < Imin)
			Imin = I(x,y,z);
	}

	cimg_forXYZ(I,x,y,z)
		I(x,y,z) = (I(x,y,z) - Imin) / (Imax - Imin) * (max - min) + min;
}

void ImgTools::structure_texture_rof(CImg<double> &structure, CImg<double> &texture, CImg<double> &I, double theta, int nb_iters, double alp)
{
	// Rescale to [-1 1]
	CImg<double> rescaled_I = ImgTools::get_rgb2gray(I);
	rescaled_I = ImgTools::get_rescale(rescaled_I,-1,1);

	// Step size
	double delta = (double)1/(4*theta);

	// Initialize dual variable p to be 0
	CImg<double> p1(rescaled_I.width(), rescaled_I.height(),1,1,0), p2(rescaled_I.width(), rescaled_I.height(),1,1,0);

	// Gradient descent
	CImg<double> div_p, I_x, I_y,reprojection;
	CImg<double> div_filter(3,1,1,1,   -1,1,0);
	CImg<double> grad_filter(2,1,1,1,   -1,1);
	for(int i=0; i<nb_iters; i++)
	{
		// Compute divergence
		div_p = p1.get_correlate(div_filter) + p2.get_correlate(div_filter.get_transpose());

		I_x = rescaled_I + theta * div_p; I_y = I_x;
		I_x.correlate(grad_filter);
		I_y.correlate(grad_filter.get_transpose());

		// Update dual variable
		p1 += I_x * delta;
		p2 += I_y * delta;

		// Reproject to |p| <= 1
		reprojection = ((p1.get_pow(2) + p2.get_pow(2)).get_sqrt()).get_max(1);
		cimg_forXY(p1,x,y)
		{
			p1(x,y) /= reprojection(x,y);
			p2(x,y) /= reprojection(x,y);
		}
	}
	// Compute divergence
	div_p = p1.get_correlate(div_filter) + p2.get_correlate(div_filter.get_transpose());

	// Compute structure and texture components
	structure = rescaled_I + theta * div_p;

	texture = rescaled_I - structure * alp;

	ImgTools::rescale(structure,0,255);
	ImgTools::rescale(texture,0,255);
}

double ImgTools::distance(CImg<double> &I1, CImg<double> &I2)
{
	double distance = 0;
	cimg_forXY(I1,x,y)
		distance += abs(I1(x,y) - I2(x,y));

	return distance;
}

double ImgTools::distance_imgs(CImg<double> &I1, CImg<double> &I2, int type)
{
	if(I1.width() != I2.width() || I1.height() != I2.height())
		cout << "distance_imgs : images must have the same size !!" << endl;

	double d = 0;
	if(type == 1)
	{
		cimg_forXY(I1,x,y)
			d += abs(I1(x,y) - I2(x,y));
	}
	else if(type == 2)
	{
		double spatial_var = I1.width()/3, color_var = 50;
		int xc = I1.width()/2, yc = I1.height()/2;
		double val_I1, val_I2;
		double weight;
		cimg_forXY(I1,x,y)
		{
			weight = exp(-(sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc))/spatial_var + abs(I1(x,y) - I1(xc,yc))/color_var));
			val_I1 = I1(x,y) * weight;
			weight = exp(-(sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc))/spatial_var + abs(I2(x,y) - I2(xc,yc))/color_var));
			val_I2 = I2(x,y) * weight;
			d += abs(val_I1 - val_I2);
		}
	}
	else if(type == 3)
	{
		CImg<double> histo1 = I1.get_histogram(256), histo2 = I2.get_histogram(256);
		cimg_forX(histo1,x)
			d += abs(histo1(x) - histo2(x));
	}
	else if(type == 4)
	{
		double mean1 = I1.mean(), mean2 = I2.mean();
		double std1 = sqrt(I1.variance()), std2 = sqrt(I2.variance());
		double spatial_var = I1.width()/3, color_var = 50;
		int xc = I1.width()/2, yc = I1.height()/2;
		double val_I1, val_I2;
		double weight;
		cimg_forXY(I1,x,y)
		{
			weight = exp(-(sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc))/spatial_var + abs(I1(x,y) - I1(xc,yc))/color_var));
			val_I1 = I1(x,y) * weight;
			weight = exp(-(sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc))/spatial_var + abs(I2(x,y) - I2(xc,yc))/color_var));
			val_I2 = I2(x,y) * weight;
			d += (val_I1 - mean1) * (val_I2 - mean2);
		}
		d /= (std1*std2);
		d = 1-d;
	}
}

double ImgTools::distance_patchs(CImg<double> &I1, CImg<double> &I2, int cx, int cy, int dx, int dy, int size, int type, double spatial_var, double color_var)
{
	double d = 0;
	int width = I1.width(), height = I1.height();
	if(type == 1) // L1
	{
		int norm=0;
		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
			if(x>0 && x<width && y>0 && y<height)
			{
				d += abs(I1(x,y) - I2(x+dx,y+dy));
				norm++;
			}
		d/=norm;
	}
	else if(type == 2) //L1 + bilateral filter
	{
		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
			d += abs(I1(x,y) * exp(-(sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy))/spatial_var + abs(I1(x,y) - I1(cx,cy))/color_var)) - I2(x+dx,y+dy) * exp(-(sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy))/spatial_var + abs(I2(x+dx,y+dy) - I2(cx+dx,cy+dy))/color_var)));
	}
	else if(type == 3) //L1 + bilateral filter - Yoon06
	{
		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
			d += abs(I1(x,y) - I2(x+dx,y+dy)) * exp(-(sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy))/spatial_var + abs(I1(x,y) - I1(cx,cy))/color_var)) * exp(-(sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy))/spatial_var + abs(I2(x+dx,y+dy) - I2(cx+dx,cy+dy))/color_var));
	}
//	else if(type == 3) // Histogram matching
//	{;
//		CImg<double> histo1 = I1.get_histogram(256), histo2 = I2.get_histogram(256);
//		cimg_forX(histo1,x)
//			d += abs(histo1(x) - histo2(x));
//	}
	else if(type == 4) // Correlation + bilateral fliter
	{
		double mean1=0, mean2=0;
		//Computation of the mean and variance
		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
			if(x>0 && x<width && y>0 && y<height && x+dx>0 && x+dx<width && y+dy>0 && y+dy<height)
			{
				mean1 += I1(x,y);
				mean2 += I2(x+dx,y+dy);
			}

		double std1=0, std2=0;
		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
			if(x>0 && x<width && y>0 && y<height && x+dx>0 && x+dx<width && y+dy>0 && y+dy<height)
			{
				std1 += (I1(x,y) - mean1)*(I1(x,y) - mean1);
				std2 += (I2(x+dx,y+dy) - mean2)*(I2(x+dx,y+dy) - mean2);
			}

		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
			if(x>0 && x<width && y>0 && y<height && x+dx>0 && x+dx<width && y+dy>0 && y+dy<height)
			d += (I1(x,y) - mean1) * (I2(x+dx,y+dy) - mean2);
		d /= sqrt(std1*std2);
		d = 1-d;
//		cout << d << endl;
	}
	else if(type == 5) // Correlation + bilateral fliter
	{
		int nb_pixs = size*size;
		double mean1=0, mean2=0;
		//Computation of the mean and variance
		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
		{
			mean1 += I1(x,y);
			mean2 += I2(x+dx,y+dy);
		}
		mean1 /= nb_pixs;
		mean2 /= nb_pixs;

		double std1=0, std2=0;
		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
		{
			std1 += (I1(x,y) - mean1)*(I1(x,y) - mean1);
			std2 += (I2(x+dx,y+dy) - mean2)*(I2(x+dx,y+dy) - mean2);
		}
		std1 /= nb_pixs;
		std2 /= nb_pixs;
		std1 = sqrt(std1);
		std2 = sqrt(std2);

		for(int x=cx - size/2; x<cx + size/2; x++)
		for(int y=cy - size/2; y<cy + size/2; y++)
			d += (I1(x,y) * exp(-(sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy))/spatial_var + abs(I1(x,y) - I1(cx,cy))/color_var)) - mean1) * (I2(x+dx,y+dy) * exp(-(sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy))/spatial_var + abs(I2(x+dx,y+dy) - I2(cx+dx,cy+dy))/color_var)) - mean2);
		d /= (std1*std2);
		d = 1-d;
	}

	return d;
}

double ImgTools::median(vector<double> &vect)
{
	sort(vect.begin(), vect.end());
	return vect[floor(vect.size()/2)];
}

CImg<double> ImgTools::min_var(vector < CImg<double> > & vect_imgs, vector<int> &vect_inds_x, vector<int> &vect_inds_y, int &i, int &j)
{
	double min_variance=10000;
	for(int k=0; k<vect_imgs.size(); k++)
	{
		if(ImgTools::get_variance(vect_imgs[k]) < min_variance)
		{
			min_variance = ImgTools::get_variance(vect_imgs[k]);
			i = vect_inds_x[k];
			j = vect_inds_y[k];
		}
	}
}

double ImgTools::vector_mult(CImg<double> &vect1, CImg<double> &vect2)
{
	if(vect1.height() != vect2.height())
		cout << "error in vector_mult : vector must have the same size!" << endl;
	if(vect1.width() != 1 || vect2.width() != 1)
		cout << "error in vector_mult : arguments must be vectors!" << endl;

	double mult = 0;
	for (int i = 0; i<vect1.height(); i++)
		mult += vect1(i) * vect2(i);

	return mult;
}

void ImgTools::cimg_mult(CImg<double> &I1, CImg<double> &I2, CImg<double>&I_out)
{
	cimg_forXY(I1,x,y)
		I_out(x,y) = I1(x,y) * I2(x,y);
}

void ImgTools::cimg_mult(CImg<double> &I1, CImg<double> &I2, CImg<double> &I3, CImg<double>&I_out)
{
	cimg_forXY(I1,x,y)
		I_out(x,y) = I1(x,y) * I2(x,y) * I3(x,y);
}

void ImgTools::occlusions_zero(CImg<double> &u)
{
	cimg_forXY(u,x,y)
		if(u(x,y) == OCCLUSION)
		{
			cout << "a" << endl;
			u(x,y) = 0;
		}
}

void ImgTools::gaussian_weights(CImg<double> &I)
{
	CImg<double> tmp(I);
	double std = (min(I.width(), I.height())-1);
	double a = 1/(2*pi*std*std);
	int cx = (I.width()-1)/2, cy = (I.height()-1)/2;
	cimg_forXY(I,x,y)
	{
		tmp(x,y) = a * exp(-((x-cx)*(x-cx) + (y-cy)*(y-cy))/(2*std*std));
		I(x,y) *= a * exp(-((x-cx)*(x-cx) + (y-cy)*(y-cy))/(2*std*std));
//		if(weight(x,y) < 0.5)
//			weight(x,y) = 0.5;
	}
//	CImgDisplay disp(tmp);
//	disp.resize(300,300);
//	while (!disp.is_closed())
//		disp.wait();

}

void ImgTools::inverse_gaussian_weights(CImg<double> &I)
{
	CImg<double> tmp(I);
	double width = I.width(), height = I.height();
	double std = (min(I.width(), I.height()));
	double a = 1/(2*pi*std*std);
	int cx = (I.width()-1)/2, cy = (I.height()-1)/2;
	cimg_forXY(I,x,y)
	{
		if(abs(x-cx) > width/2.5 || abs(y-cy) > height/2.5)
			I(x,y) += I(x,y)/2;
//		tmp(x,y) = 2 - a * exp(-((x-cx)*(x-cx) + (y-cy)*(y-cy))/(2*std*std));
//		I(x,y) *= 2 - a * exp(-((x-cx)*(x-cx) + (y-cy)*(y-cy))/(2*std*std));
//		if(weight(x,y) < 0.5)
//			weight(x,y) = 0.5;
	}
//	CImgDisplay disp(I);
//	disp.resize(300,300);
//	while (!disp.is_closed())
//		disp.wait();

}

void ImgTools::extend(CImg<double> &I, CImg<double> &I_extend, int w)
{
	int width = I.width(), height = I.height();
	int width_ex = I.width()+2, height_ex = I.height()+2;

	I_extend.assign(width_ex, height_ex, I.depth(), I.spectrum());

	// Inside
	cimg_for_insideXY(I_extend,x,y,1)
		I_extend(x,y) = I(x-1,y-1);

	// Borders
	cimg_for_borderXY(I_extend,x,y,1)
	{
		// Corners
		if(x==0 && y==0)
			I_extend(x,y) = I(0,0);
		else if(x==0 && y>=height_ex-1)
			I_extend(x,y) = I(0,height-1);
		else if(x==width_ex-1 && y==0)
			I_extend(x,y) = I(width-1,0);
		else if(x==width_ex-1 && y==height_ex-1)
			I_extend(x,y) = I(width-1, height-1);

		// Borders
		else if(x==0)
			I_extend(x,y) = I(0,y-1);
		else if(x>=width_ex-w)
			I_extend(x,y) = I(width-1,y-1);
		else if(y==0)
			I_extend(x,y) = I(x-1,0);
		else if(y>=height_ex-w)
			I_extend(x,y) = I(x-1,height-1);
	}
}

bool ImgTools::include(int width, int height, int x, int y)
{
	return (x>=0 && x<width && y>=0 && y<height);
}

void ImgTools::gray2rgb(CImg<double> &I_gray, CImg<double> &I_rgb)
{
	I_rgb.assign(I_gray.width(), I_gray.height(), I_gray.depth(), 3);
	cimg_forXY(I_rgb,x,y)
	{
		I_rgb(x,y,0,0) = I_gray(x,y);
		I_rgb(x,y,0,1) = I_gray(x,y);
		I_rgb(x,y,0,2) = I_gray(x,y);
	}
}

void ImgTools::histogram_cumul(CImg<double> &I, CImg<double> &histo_cumul, int nb_bins, double val_min, double val_max, bool normalized)
{
	CImg<double> histo = I.get_histogram(nb_bins,val_min, val_max);
	int N;
	if(normalized == 0)
		N = 1;
	else
		N = I.width() * I.height();

	histo_cumul.assign(nb_bins,1,1,1,0);
	histo_cumul(0)=histo(0);
	for(int i=1; i<nb_bins; i++)
		histo_cumul(i) = histo_cumul(i-1) + histo(i);

	histo_cumul /= N;
}

void ImgTools::histogram_equal(CImg<double> &I)
{
	CImg<double> H;
	ImgTools::histogram_cumul(I,H);
	cimg_forXY(I,x,y)
		I(x,y) = H(I(x,y));
}

void ImgTools::histogram_specif(CImg<double> &Iref, CImg<double> &I)
{
	CImg<double> histo_cumul_Iref, histo_cumul_I;
	ImgTools::histogram_cumul(I,histo_cumul_I);
	ImgTools::histogram_cumul(Iref,histo_cumul_Iref);

	cimg_forXY(I,x,y)
	{
		for(int i=0; i<256; i++)
			if(histo_cumul_Iref(i) == histo_cumul_I(I(x,y)))
				I(x,y) = i;
	}
}

void ImgTools::midway_equalization(CImg<double> &I1, CImg<double> &I2)
{
	CImg<double> H1, H2;
	int Imax = max(I1.max(), I2.max());
	ImgTools::histogram_cumul(I1,H1,Imax+10, 0, Imax+9,1);
	ImgTools::histogram_cumul(I2,H2,Imax+10, 0, Imax+9,1);

	double H1_xy, H2_xy, H1_inv, H2_inv;
	cimg_forXY(I1,x,y)
	{
		// Transformation of I1
		H1_xy = H1(I1(x,y));
		int i = 0;
		while(H2(i) < H1_xy)
		{
			H2_inv = i;
			i++;
		}
		I1(x,y) = (I1(x,y) + H2_inv)/2;

		// Transformation of I2
		H2_xy = H2(I2(x,y));
		i = 0;
		while(H1(i) < H2_xy)
		{
			H1_inv = i;
			i++;
		}
		I2(x,y) = (I2(x,y) + H1_inv)/2;
	}
}

void ImgTools::get_midway_equalization(CImg<double> &I1, CImg<double> &I2, CImg<double> &I1_eq, CImg<double> &I2_eq)
{
	CImg<double> H1, H2;
	int Imax = max(I1.max(), I2.max());
	ImgTools::histogram_cumul(I1,H1,Imax+1, 0, Imax,1);
	ImgTools::histogram_cumul(I2,H2,Imax+1, 0, Imax,1);
	I1_eq.assign(I1.width(), I1.height(), 1, 1);
	I2_eq.assign(I1.width(), I1.height(), 1, 1);

	double H1_xy, H2_xy, H1_inv, H2_inv;
	cimg_forXY(I1,x,y)
	{
		// Transformation of I1
		H1_xy = H1(I1(x,y));
		int i = 0;
		while(H2(i) < H1_xy)
		{
			H2_inv = i;
			i++;
		}
		I1_eq(x,y) = (I1(x,y) + H2_inv)/2;

		// Transformation of I2
		H2_xy = H2(I2(x,y));
		i = 0;
		while(H1(i) < H2_xy)
		{
			H1_inv = i;
			i++;
		}
		I2_eq(x,y) = (I2(x,y) + H1_inv)/2;
	}
}

void ImgTools::get_midway_equalization(CImgList<double> &I, CImgList<double> &I_eq, int N)
{
	int nb_frames = I.size(), width = I(0).width(), height = I(0).height();
	I_eq.assign(nb_frames);
	CImg<double> Ht, Htmp;
	CImgList<double> Hs(2*N+1);
	int Imax=0, i;
	double Hs_inv;
	for(int t=0; t<nb_frames; t++)
	{cout << t << endl;
		int k_beg = max(t-N,0), k_end = min(t+N,nb_frames);
		// Computation of Imax
		Imax = 0;
		for(int k=k_beg; k<k_end; k++)
			if(I(k).max() > Imax)
				Imax = I(k).max();

		// Computation of the histograms
		ImgTools::histogram_cumul(I(t),Ht,Imax+1, 0, Imax,1);
		for(int k=k_beg; k<k_end; k++)
		{
			ImgTools::histogram_cumul(I(k),Htmp,Imax+1, 0, Imax,1);
			Hs(k-k_beg).assign(Htmp);
		}

		// Midway equalization
		I_eq(t).assign(width,height,1,1,1);
		cimg_forXY(I_eq(t),x,y)
			for(int k=k_beg; k<k_end; k++)
			{
				i = 0;
				while(Hs(k-k_beg)(i) < Ht(I(t)(x,y)))
				{
					Hs_inv = i;
					i++;
				}
				I_eq(t)(x,y) += Hs_inv;
			}
		I_eq(t) /= k_end-k_beg;
	}
}

void ImgTools::get_midway_equalization_tiff(CImg<double> &Itiff, CImg<double> &Itiff_eq, int N)
{
	int nb_frames = Itiff.depth();

	// Convert the tiff stack into CImgList
	CImgList<double> I(nb_frames),I_eq(nb_frames);
	for(int i=0; i<nb_frames; i++)
	{
		I(i).assign(Itiff.width(), Itiff.height(),1,1);
		cimg_forXY(Itiff,x,y)
			I(i)(x,y) = Itiff(x,y,i);
	}
	ImgTools::get_midway_equalization(I,I_eq,N);

	// Convert the CImgList into tiff stack
	Itiff_eq.assign(Itiff);
	for(int i=0; i<nb_frames; i++)
		cimg_forXY(Itiff,x,y)
			Itiff_eq(x,y,i) = I_eq(i)(x,y);
}

//void ImgTools::get_histogram_midway_flim_12(CImgList<double> &Iflim, CImgList<double> &Iflim_eq)
//{
//	CImgList<double> Hi;
//	int nb_frames = Hi.size();
//	CImg<double> Htmp;
//	for(int i=0; i<nb_frames; i++)
//	{
//	ImgTools::histogram_cumul(I1,Hi,pow(2,13));
//	ImgTools::histogram_cumul(I2,H2,pow(2,13));
//	I1_eq.assign(I1.width(), I1.height(), 1, 1);
//	I2_eq.assign(I1.width(), I1.height(), 1, 1);
//
//	double H1_xy, H2_xy, H1_inv, H2_inv;
//	cimg_forXY(I1,x,y)
//	{
//		// Transformation of I1
//		H1_xy = H1(I1(x,y));
//		int i = 0;
//		while(H2(i) < H1_xy)
//		{
//			H2_inv = i;
//			i++;
//		}
//		I1_eq(x,y) = (I1(x,y) + H2_inv)/2;
//
//		// Transformation of I2
//		H2_xy = H2(I2(x,y));
//		i = 0;
//		while(H1(i) < H2_xy)
//		{
//			H1_inv = i;
//			i++;
//		}
//		I2_eq(x,y) = (I2(x,y) + H1_inv)/2;
//	}
//}


void ImgTools::threshold_min(CImg<double> &I, double threshold)
{
	cimg_forXY(I,x,y)
		if(I(x,y) < threshold)
			I(x,y) = threshold;
}

void ImgTools::compute_bilateral_weights(CImg<double> &I, double alpha, double beta, CImg<double> &w)
{
	int width = I.width(), height = I.height();
	w.assign(width,height,1,1,0);
	int cx = (width - 1)/2, cy = (height - 1)/2;
	double delta_s, delta_c;
	cimg_forXY(w,x,y)
	{
		delta_s = sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy));
		delta_c = abs(I(x,y) - I(cx,cy));
		w(x,y) = exp(-delta_c/alpha - delta_s/beta);
	}
}


void ImgTools::compute_bilateral_weights(CImg<double> &u, CImg<double> &v, double alpha, double beta, CImg<double> &w)
{
	int width = u.width(), height = u.height();
	w.assign(width,height,1,1,0);
	int cx = (width - 1)/2, cy = (height - 1)/2;
	double delta_s, delta_c;
	cimg_forXY(w,x,y)
	{
		if(u(x,y) == -1)
			w(x,y) = 0;
		else
		{
			delta_c = sqrt((u(x,y)-u(cx,cy))*(u(x,y)-u(cx,cy)) + (v(x,y)-v(cx,cy))*(v(x,y)-v(cx,cy)));
			delta_s = sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy));
			w(x,y) = exp(-delta_c/alpha - delta_s/beta);
		}
	}
}

void ImgTools::set_gaussian_weights(CImg<double> &I, double std)
{
	int cx = (I.width()-1)/2, cy = (I.height()-1)/2;
	cimg_forXY(I,x,y)
		I(x,y) = I(x,y) * 1/(sqrt(2*pi)*std) * exp(-((x-cx)*(x-cx) + (y-cy)*(y-cy)) / (2*std*std));
}


////////////////////////////	Warping IPOL

//0 Neumann
//1 Periodic
//2 Symmetric

int ImgTools::neumann_bc_ipol(int x, int nx, double *mask)
{

	if(x < 0)
	{
		x = 0;
		*mask = 0;
	}
	else if (x >= nx){
		x = nx - 1;
		*mask = 0;
	}

	return x;
}

int ImgTools::periodic_bc_ipol(int x, int nx, double *mask)
{
	if(x < 0) {
		const int n   = 1 - (int)(x/(nx+1));
		const int ixx = x + n * nx;

		x =   ixx% nx;
		*mask = 0;
	}
	else if(x >= nx) {
		x = x % nx;
		*mask = 0;
	}

	return x;
}


int ImgTools::symmetric_bc_ipol(int x, int nx, double *mask)
{
	if(x < 0) {
		const int borde = nx - 1;
		const int xx = -x;
		const int n  = (int)(xx/borde) % 2;

		if ( n ) x = borde - ( xx % borde );
		else x = xx % borde;
		*mask = 0;
	}
	else if ( x >= nx ) {
		const int borde = nx - 1;
		const int n = (int)(x/borde) % 2;

		if ( n ) x = borde - ( x % borde );
		else x = x % borde;
		*mask = 0;
	}

	return x;
}

/**
  *
  * Bicubic interpolation in one dimension
  *
**/
double ImgTools::cubic_interpolation (
    double v[4],  ///interpolation points
    double x      ///point to be interpolated
)
{
	return v[1] + 0.5 * x*(v[2] - v[0]
                + x*(2.0*v[0] - 5.0*v[1] + 4.0*v[2] - v[3]
                + x*(3.0*(v[1] - v[2]) + v[3] - v[0])));
}

/**
  *
  * Bicubic interpolation in two dimension
  *
**/
double ImgTools::bicubic_interpolation (
    double p[4][4], ///array containing the interpolation points
    double x,       ///x position to be interpolated
    double y        ///y position to be interpolated
)
{
	double v[4];
	v[0] = cubic_interpolation(p[0], y);
	v[1] = cubic_interpolation(p[1], y);
	v[2] = cubic_interpolation(p[2], y);
	v[3] = cubic_interpolation(p[3], y);
	return cubic_interpolation(v, x);
}

/**
  *
  * Calculate the bicubic interpolation of a point in
  * an image. It also detects if the point goes beyond the
  * image limits
  *
**/
double ImgTools::bicubic_interpolation(
    const double *input, ///image to be interpolated
    const double uu,     ///x component of the vector field
    const double vv,     ///y component of the vector field
    const int   nx,     ///width of the image
    const int   ny,     ///height of the image
    double       &mask   ///mask to detect the motions outside the image
)
{
    const int sx = (uu < 0)? -1: 1;
    const int sy = (vv < 0)? -1: 1;

    int x, y, mx, my, dx, dy, ddx, ddy;

    switch(BOUNDARY_CONDITION) {

    case 0:x   = neumann_bc_ipol((int) uu, nx, &mask);
           y   = neumann_bc_ipol((int) vv, ny, &mask);
           mx  = neumann_bc_ipol((int) uu - sx, nx, &mask);
           my  = neumann_bc_ipol((int) vv - sx, ny, &mask);
           dx  = neumann_bc_ipol((int) uu + sx, nx, &mask);
           dy  = neumann_bc_ipol((int) vv + sy, ny, &mask);
           ddx = neumann_bc_ipol((int) uu + 2*sx, nx, &mask);
           ddy = neumann_bc_ipol((int) vv + 2*sy, ny, &mask);
           break;

    case 1:x   = periodic_bc_ipol((int) uu, nx, &mask);
           y   = periodic_bc_ipol((int) vv, ny, &mask);
           mx  = periodic_bc_ipol((int) uu - sx, nx, &mask);
           my  = periodic_bc_ipol((int) vv - sx, ny, &mask);
           dx  = periodic_bc_ipol((int) uu + sx, nx, &mask);
           dy  = periodic_bc_ipol((int) vv + sy, ny, &mask);
           ddx = periodic_bc_ipol((int) uu + 2*sx, nx, &mask);
           ddy = periodic_bc_ipol((int) vv + 2*sy, ny, &mask);
           break;

    case 2:x   = symmetric_bc_ipol((int) uu, nx, &mask);
           y   = symmetric_bc_ipol((int) vv, ny, &mask);
           mx  = symmetric_bc_ipol((int) uu - sx, nx, &mask);
           my  = symmetric_bc_ipol((int) vv - sx, ny, &mask);
           dx  = symmetric_bc_ipol((int) uu + sx, nx, &mask);
           dy  = symmetric_bc_ipol((int) vv + sy, ny, &mask);
           ddx = symmetric_bc_ipol((int) uu + 2*sx, nx, &mask);
           ddy = symmetric_bc_ipol((int) vv + 2*sy, ny, &mask);
           break;

    default:x   = neumann_bc_ipol((int) uu, nx, &mask);
            y   = neumann_bc_ipol((int) vv, ny, &mask);
            mx  = neumann_bc_ipol((int) uu - sx, nx, &mask);
            my  = neumann_bc_ipol((int) vv - sx, ny, &mask);
            dx  = neumann_bc_ipol((int) uu + sx, nx, &mask);
            dy  = neumann_bc_ipol((int) vv + sy, ny, &mask);
            ddx = neumann_bc_ipol((int) uu + 2*sx, nx, &mask);
            ddy = neumann_bc_ipol((int) vv + 2*sy, ny, &mask);
            break;
    }

    //obtain the interpolation points of the image
    const double p11 = input[mx  + nx * my];
    const double p12 = input[x   + nx * my];
    const double p13 = input[dx  + nx * my];
    const double p14 = input[ddx + nx * my];

    const double p21 = input[mx  + nx * y];
    const double p22 = input[x   + nx * y];
    const double p23 = input[dx  + nx * y];
    const double p24 = input[ddx + nx * y];

    const double p31 = input[mx  + nx * dy];
    const double p32 = input[x   + nx * dy];
    const double p33 = input[dx  + nx * dy];
    const double p34 = input[ddx + nx * dy];

    const double p41 = input[mx  + nx * ddy];
    const double p42 = input[x   + nx * ddy];
    const double p43 = input[dx  + nx * ddy];
    const double p44 = input[ddx + nx * ddy];

    //create array
    double pol[4][4] = {{p11,p21,p31,p41}, {p12,p22,p32,p42}, {p13,p23,p33,p43}, {p14,p24,p34,p44}};

    return bicubic_interpolation(pol, (double)uu-x, (double)vv-y);
}

/**
  *
  * Calculate the bicubic interpolation of an image.
  * It also creates a mask for the points that go beyond the
  * image limits
  *
**/
void ImgTools::bicubic_interpolation(
		CImg<double> &input_img,		//image to be warped
		CImg<double> &u_img,		 	//x component of the vector field
		CImg<double> &v_img,		 	//y component of the vector field
		CImg<double> &mask_img,         //mask to detect the motions outside the image
		CImg<double> &output_img
		)
{
	int nx = input_img.width();
	int ny = input_img.height();
	int size = nx*ny;
//	double *input = (double*)malloc(size); input = (double*)input_img.data();
//	double *u = (double*)malloc(size); u = (double*)u_img.data();
//	double *v = (double*)malloc(size); v = (double*)v_img.data();
	double *input = (double*)input_img.data();
	double *u = (double*)u_img.data();
	double *v = (double*)v_img.data();
	double *mask   = new double[size];
	double *output = new double[size];

	for(int i = 0; i < nx * ny; i++)
        mask[i] = 1;

    for(int j = 0; j < ny; j++)

       for(int i = 0; i < nx; i++)
       {
          const int   p = i + nx * j;
          const double uu = (double) (i + u[p]);
          const double vv = (double) (j + v[p]);

          output[p] = bicubic_interpolation(input, uu, vv, nx, ny, mask[p]);
//		  output[i + nx * j] = input_img.linear_atXY(i+u_img(i,j), j+v_img(i,j));
       }
	output_img.assign(output, input_img.width(), input_img.height(), 1, 1);
	mask_img.assign(mask, input_img.width(), input_img.height(), 1, 1);

	delete output;
	delete mask;
}

/**
 *
 * Function to warp the image using bilinear or bicubic interpolation
 *
 **/
void ImgTools::bilinear_interpolation(
	CImg<double> &input_img,		//image to be warped
	CImg<double> &u_img,		 	//x component of the vector field
	CImg<double> &v_img,		 	//y component of the vector field
	CImg<double> &mask_img,         //mask to detect the motions outside the image
	CImg<double> &output_img
)
{
	int nx = input_img.width();
	int ny = input_img.height();
	int size = nx*ny;
//	double *input = (double*)malloc(size); input = (double*)input_img.data();
//	double *u = (double*)malloc(size); u = (double*)u_img.data();
//	double *v = (double*)malloc(size); v = (double*)v_img.data();
	double *input = (double*)input_img.data();
	double *u = (double*)u_img.data();
	double *v = (double*)v_img.data();
	double *mask   = new double[size];
	double *output = new double[size];

	for(int i = 0; i < nx * ny; i++)
		mask[i] = 1;

	{
		for(int j = 0; j < ny; j++)

			for(int i = 0; i < nx; i++)
			{

				const double uu = (double) (i + u[i + nx * j]);
				const double vv = (double) (j + v[i + nx * j]);

				const int sx = (uu < 0)? -1: 1;
				const int sy = (vv < 0)? -1: 1;
				const int p  = i + nx * j;

				int x, y, dx, dy;

				switch(BOUNDARY_CONDITION) {

				case 0:x  = neumann_bc_ipol((int) uu, nx, mask+p);
				       y  = neumann_bc_ipol((int) vv, ny, mask+p);
				       dx = neumann_bc_ipol(x + sx, nx,   mask+p);
				       dy = neumann_bc_ipol(y + sy, ny,   mask+p);
				       break;

				case 1:x  = periodic_bc_ipol((int) uu, nx, mask+p);
				       y  = periodic_bc_ipol((int) vv, ny, mask+p);
				       dx = periodic_bc_ipol(x + sx, nx,   mask+p);
				       dy = periodic_bc_ipol(y + sy, ny,   mask+p);
				       break;

				case 2:x  = symmetric_bc_ipol((int) uu, nx, mask+p);
				       y  = symmetric_bc_ipol((int) vv, ny, mask+p);
				       dx = symmetric_bc_ipol(x + sx, nx,   mask+p);
				       dy = symmetric_bc_ipol(y + sy, ny,   mask+p);
				       break;

				default:x  = neumann_bc_ipol((int) uu, nx, mask+p);
					y  = neumann_bc_ipol((int) vv, ny, mask+p);
					dx = neumann_bc_ipol(x + sx, nx,   mask+p);
					dy = neumann_bc_ipol(y + sy, ny,   mask+p);
					break;
				}
//				const double p1 = input[x  + nx * y];
//				  const double p2 = input[dx + nx * y];
//				  const double p3 = input[x  + nx * dy];
//				  const double p4 = input[dx + nx * dy];
//
//				  const double e1 = ((double) sx * (uu - x));
//				  const double E1 = ((double) 1.0 - e1);
//				  const double e2 = ((double) sy * (vv - y));
//				  const double E2 = ((double) 1.0 - e2);
//
//				  const double w1 = E1 * p1 + e1 * p2;
//				  const double w2 = E1 * p3 + e1 * p4;
//
//				  output[i + nx * j] = E2 * w1 + e2 * w2;
				  output[i + nx * j] = input_img.linear_atXY(i+u_img(i,j), j+v_img(i,j));
			}
	}
	output_img.assign(output, input_img.width(), input_img.height(), 1, 1);
	mask_img.assign(mask, input_img.width(), input_img.height(), 1, 1);

	delete output;
	delete mask;
}

CImg<double> ImgTools::get_warp_ipol(
	CImg<double> &input_img,		//image to be warped
	CImg<double> &u_img,		 	//x component of the vector field
	CImg<double> &v_img,		 	//y component of the vector field
	CImg<double> &mask_img,         //mask to detect the motions outside the image
	const char *type
	)
{
	CImg<double> output_img;

	if(type == "bilinear")
		bilinear_interpolation(input_img, u_img, v_img, mask_img, output_img);
	if(type == "bicubic")
		bicubic_interpolation(input_img, u_img, v_img, mask_img, output_img);

	return output_img;
}
